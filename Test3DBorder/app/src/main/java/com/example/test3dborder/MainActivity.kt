package com.example.test3dborder

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Search
import androidx.compose.material3.Icon
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.test3dborder.ui.theme.Test3DBorderTheme
import com.example.test3dborder.utilities.convexBorder
import com.example.test3dborder.widgets.BasicTextFieldDemo

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Test3DBorderTheme {
                // A surface container using the 'background' color from the theme
                Surface(
//                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    MyApp()
//                    BasicTextFieldDemo() //for testing purposes.
                }
            }
        }
    }
}

@Composable
fun MyApp() {
    // Mutable state to hold the text input
    var text by remember { mutableStateOf("hello") }

    BasicTextField(
        value = text,
        onValueChange = { text = it },
        singleLine = true,
        keyboardOptions = KeyboardOptions(
            capitalization = KeyboardCapitalization.Sentences,
            imeAction = ImeAction.Search
        ),
        textStyle = LocalTextStyle.current.copy(
            fontSize = 16.sp,
            fontWeight = FontWeight.Medium
        ),
        decorationBox = { innerTextField ->
            Row(
                modifier = Modifier
                    .size(300.dp, 60.dp)
//                    // Set the background color and shape
//                    .background(Color(0xFF7F2DBF), CircleShape)
//                    .background(MaterialTheme.colorScheme.primary, CircleShape)
                    .background(colorResource(id = R.color.soft_blue), CircleShape)

//                    // Apply the convex border with the same color and shape
//                    .convexBorder(Color(0xFF7F2DBF), CircleShape)
                    .convexBorder(colorResource(id = R.color.soft_blue), CircleShape)
                    .padding(horizontal = 20.dp),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.spacedBy(8.dp)
            ) {
                // Add a search icon
                Icon(
                    imageVector = Icons.Rounded.Search,
                    contentDescription = null
                )
                Box {
                    // Show placeholder text when the input text is empty
                    if (text.isEmpty()) {
                        Text(
                            text = "Search...",
                            style = LocalTextStyle.current.copy(color = Color(0xFF242424))
                        )
                    }
                    // Display the actual text field
                    innerTextField()
                }
            }
        }
    )
    Log.d("any", "MyApp: $text")
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    Test3DBorderTheme {
        MyApp()
//        BasicTextFieldDemo() //for testing purposes.
    }
}