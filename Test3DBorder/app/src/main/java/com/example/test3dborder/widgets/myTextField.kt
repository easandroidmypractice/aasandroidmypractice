package com.example.test3dborder.widgets

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.test3dborder.R

@Preview
@Composable
fun BasicTextFieldDemo() {
//    var textState by remember { mutableStateOf(TextFieldValue("Hello World")) }
//    Column(modifier = Modifier.background(colorResource(id = R.color.purple_200))) {
//
//        BasicTextField(value = textState, onValueChange = {
//            textState = it
//        })
//        Text("The textField has this text: " + textState.text)
//    }

    var username by rememberSaveable { mutableStateOf("hellooooooo") }
    Row {
        BasicTextField(
            modifier = Modifier
                .size(300.dp, 60.dp)
                .background(colorResource(id = R.color.soft_blue)),
            value = username,
            onValueChange = { username = it },
        )
    }
}



