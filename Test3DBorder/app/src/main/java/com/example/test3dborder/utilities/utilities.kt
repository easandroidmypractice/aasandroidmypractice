package com.example.test3dborder.utilities

import android.graphics.BlurMaskFilter
import android.graphics.PorterDuff
import android.graphics.PorterDuffXfermode
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.geometry.toRect
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Outline
import androidx.compose.ui.graphics.Paint
import androidx.compose.ui.graphics.PaintingStyle
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.graphics.drawOutline
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.graphics.drawscope.drawIntoCanvas
import androidx.compose.ui.graphics.drawscope.translate
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.example.test3dborder.models.ConvexStyle

/* parameters:
color: the color of the border.
shape: the shape of the border.
strokeWidth: the width of the border stroke.
convexStyle: the style of the convex effect applied to the border.
 */
fun Modifier.convexBorder(
    color: Color,
    shape: Shape,
    strokeWidth: Dp = 8.dp,
    convexStyle: ConvexStyle = ConvexStyle()
) = this.drawWithContent {
    val adjustedSize = Size(size.width - strokeWidth.toPx(), size.height - strokeWidth.toPx())
    val outline = shape.createOutline(adjustedSize, layoutDirection, this)

    drawContent()

    val halfStrokeWidth = strokeWidth.toPx() / 2
    translate(halfStrokeWidth, halfStrokeWidth) {
        drawOutline(
            outline = outline,
            color = color,
            style = Stroke(width = strokeWidth.toPx())
        )
    }

    with(convexStyle) {
        drawConvexBorderShadow(outline, strokeWidth, blur, -offset, -offset, shadowColor)
        drawConvexBorderShadow(outline, strokeWidth, blur, offset, offset, glareColor)
    }
}

private fun DrawScope.drawConvexBorderShadow(
    outline: Outline,
    strokeWidth: Dp,
    blur: Dp,
    offsetX: Dp,
    offsetY: Dp,
    shadowColor: Color
) = drawIntoCanvas { canvas ->
    val shadowPaint = Paint().apply {
        this.style = PaintingStyle.Stroke
        this.color = shadowColor
        this.strokeWidth = strokeWidth.toPx()
    }

    canvas.saveLayer(size.toRect(), shadowPaint)

    val halfStrokeWidth = strokeWidth.toPx() / 2
    canvas.translate(halfStrokeWidth, halfStrokeWidth)
    canvas.drawOutline(outline, shadowPaint)

    shadowPaint.asFrameworkPaint().apply {
        xfermode = PorterDuffXfermode(PorterDuff.Mode.DST_OUT)
        maskFilter = BlurMaskFilter(blur.toPx(), BlurMaskFilter.Blur.NORMAL)
    }
    shadowPaint.color = Color.Black

    canvas.translate(offsetX.toPx(), offsetY.toPx())
    canvas.drawOutline(outline, shadowPaint)
    canvas.restore()
}
