package com.example.testgeneralcomposables.topics

import androidx.compose.foundation.border
import androidx.compose.foundation.interaction.InteractionSource
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.testgeneralcomposables.components.InputField

//@Preview
@Composable
fun TextFieldFocus() {
    Column {
        val focusManager = LocalFocusManager.current
        var text by remember { mutableStateOf("") }
        val imeAction = ImeAction.Next

        for (i in 1..2) {
            TextField(
                value = text,
                onValueChange = { text = it },
                label = { Text("Label") },
                keyboardOptions = KeyboardOptions(imeAction = imeAction),
                keyboardActions = KeyboardActions(
                    onNext = {
                        focusManager.moveFocus(FocusDirection.Down)
                    }),
            )
        }
    }
}

@OptIn(ExperimentalComposeUiApi::class)
//@Preview
@Composable //without loop
fun OutlinedTextField1(modifier: Modifier = Modifier,
                       onValChange: (String) -> Unit = {}) {
    val focusManager = LocalFocusManager.current
    var text1 = remember { mutableStateOf("") }
    var text2 = remember { mutableStateOf("") }
    val focusRequester = remember { FocusRequester() }
    val keyboardController = LocalSoftwareKeyboardController.current

    LaunchedEffect(Unit){
        focusRequester.requestFocus()
    }

    InputField(valueState = text1,
        labelId = "Field 1",
        enabled = true,
        isSingleLine = false,
        imeAction = ImeAction.Next,
        modifier = Modifier
            .focusRequester(focusRequester),
        onAction = KeyboardActions(onNext = {
            focusManager.moveFocus(FocusDirection.Down)
        }) {
            keyboardController?.hide()
        })
    InputField(valueState = text2,
        labelId = "Field 2",
        enabled = true,
        isSingleLine = false,
        imeAction = ImeAction.Done,
        onAction = KeyboardActions() {
            keyboardController?.hide()
        })
}

@OptIn(ExperimentalMaterial3Api::class)
@Preview
@Composable
fun DecoratedBasicTextField() {
    var textState by remember { mutableStateOf(TextFieldValue("Hello World")) }
    val interactionSource = remember { MutableInteractionSource() }
    Column {
        BasicTextField(
            value = textState,
            onValueChange = {
                textState = it},
            interactionSource = interactionSource,
            modifier = Modifier
                .fillMaxWidth()
                .padding(6.dp)
                .border(1.dp, Color.Gray))
        {
//            OutlinedTextFieldDefaults.DecorationBox(
//                value = value,
//                innerTextField = it,
//                singleLine = false,
//                enabled = true,
//                label = { Text("Label") },
//                placeholder = { Text("Placeholder") },
//                visualTransformation = VisualTransformation.None,
//                interactionSource = interactionSource,
//                colors = OutlinedTextFieldDefaults.colors(),
//                container = {
//                    OutlinedTextFieldDefaults.ContainerBox(
//                        enabled,
//                        isError,
//                        interactionSource,
//                        colors,
//                        shape,
//                        focusedBorderThickness = 4.dp,
//                        unfocusedBorderThickness = 4.dp
//                    )
//                }
//            )
        }
        Text("The textfield has this text: " + textState.text)
    }
}