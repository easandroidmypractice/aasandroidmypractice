package com.example.recyclerviewexample.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerviewexample.R
import com.example.recyclerviewexample.databinding.ActivityMainBinding
import com.example.recyclerviewexample.models.MainViewModel

class MainActivity : AppCompatActivity() {
//    private var binding: ActivityMainBinding? = null
    lateinit var viewModel: MainViewModel
    lateinit var activityMainBinding: ActivityMainBinding
    lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityMainBinding = ActivityMainBinding.inflate(layoutInflater)
//        toast(R.string.any_string)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        recyclerView.layoutManager = LinearLayoutManager(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        //TO avoid memory leak we disallow the binding once the activity is destroyed
//        binding = null
    }
}