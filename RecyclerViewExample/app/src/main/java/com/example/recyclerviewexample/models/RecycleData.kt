package com.example.recyclerviewexample.models

data class RecycleData(
    var name: String = "",
    var email: String = "",
    var status: Boolean
)
