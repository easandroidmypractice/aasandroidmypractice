package com.example.datatable1;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

/*
basic data table.
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}