package com.example.testingnewrecyclerview.adapters
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.testingnewrecyclerview.databinding.ItemProductsBinding
import com.example.testingnewrecyclerview.model.ProductData
import com.example.testingnewrecyclerview.viewmodel.ProductsViewModel

class ProductsItemAdapter(val temperatureLogsViewModel: ProductsViewModel):
                    ListAdapter<ProductData, RecyclerView.ViewHolder>(DiffCallback)  {

    object DiffCallback : DiffUtil.ItemCallback<ProductData>() {
        override fun areItemsTheSame(oldItem: ProductData, newItem: ProductData): Boolean {
            return oldItem.name == newItem.name
        }

        override fun areContentsTheSame(oldItem: ProductData, newItem: ProductData): Boolean {
            return oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding =
            ItemProductsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ProductsItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val adjustViewHolder = holder as ProductsItemViewHolder
        val fridgeItem = getItem(position)

        fridgeItem.let {
            adjustViewHolder.onBind(it, position)
        }
    }

    inner class ProductsItemViewHolder(private val binding: ItemProductsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind(nasaData: ProductData, position: Int) {
            binding.productsModel = nasaData
            binding.viewModel = temperatureLogsViewModel
            binding.executePendingBindings()
        }
    }
}