package com.example.testingnewrecyclerview.activities
/*
recyclerViews used in this project:
activity_main.xml (recyclerViewProducts)
adapter: ProductsItemAdapter.kt
recyclerView: recyclerViewProducts (activity_main.xml)
layout: item_products.xml
Controlled by:	MainActivity.kt
 */
import android.os.Bundle
import androidx.activity.addCallback
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import com.example.testingnewrecyclerview.R
import com.example.testingnewrecyclerview.adapters.ProductsItemAdapter
import com.example.testingnewrecyclerview.databinding.ActivityMainBinding
import com.example.testingnewrecyclerview.model.ProductData
import com.example.testingnewrecyclerview.utils.CustomBackgroundItemDecorator
import com.example.testingnewrecyclerview.utils.SpacingItemDecorator
import com.example.testingnewrecyclerview.utils.getJsonDataFromRaw
import com.example.testingnewrecyclerview.utils.observeByLambda
import com.example.testingnewrecyclerview.utils.viewModelFactory
import com.example.testingnewrecyclerview.viewmodel.ProductsViewModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class MainActivity : AppCompatActivity() {
//    private var binding: ActivityMainBinding? = null
    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    private val productsViewModel: ProductsViewModel by viewModels {viewModelFactory {
                                        ProductsViewModel(this@MainActivity)}}

    val productsItemAdapter by lazy { ProductsItemAdapter(productsViewModel)}
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding?.root)

        binding.apply {
            onBackPressedDispatcher.addCallback {finish()}

            setSupportActionBar(toolbar)
            supportActionBar?.setDisplayShowTitleEnabled(false)

            toolbar.setNavigationOnClickListener {
                onBackPressedDispatcher.onBackPressed()
            }

            getProductsItem().observeByLambda(this@MainActivity) {
                val spacingDecorator = SpacingItemDecorator(resources.getDimensionPixelSize(R.dimen.spacing_between_items))
                val customBackgroundDrawable = ContextCompat.getDrawable(this@MainActivity, R.drawable.ic_splash_background)!!
                val backgroundDecorator = CustomBackgroundItemDecorator(customBackgroundDrawable)
                recyclerViewProducts.isNestedScrollingEnabled = false
                recyclerViewProducts.adapter = productsItemAdapter
                recyclerViewProducts.addItemDecoration(spacingDecorator)//leave 5dp between items.
                /*next statement renders a bg (ic_splash_background.xml)
                for all items but the first.
                 */
                recyclerViewProducts.addItemDecoration(backgroundDecorator)
                productsItemAdapter.submitList(it)
            }
        }
        //        toast(R.string.any_string)
    }

    fun getProductsItem(): MutableLiveData<ArrayList<ProductData>> {
        val encodedString = MutableLiveData<ArrayList<ProductData>>()

        val rawFile = R.raw.products_items
        val jsonFileString = getJsonDataFromRaw(
            this,
            rawFile
        )
        val type = object : TypeToken<ArrayList<ProductData>>() {}.type
        encodedString.postValue(Gson().fromJson(jsonFileString, type))

        return encodedString
    }
}