package com.example.testoutlinedtextfield

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.testoutlinedtextfield.components.MyIOField
import com.example.testoutlinedtextfield.ui.theme.TestOutlinedTextFieldTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TestOutlinedTextFieldTheme {
                    MyApp()
                }
            }
        }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun MyApp(modifier: Modifier = Modifier,
          onValChange: (String) -> Unit = {}) {
    val totalBillState = remember {
        mutableStateOf("")
    }
    val validState = remember(totalBillState.value) {
        totalBillState.value.trim().isNotEmpty()
    }
    val keyboardController = LocalSoftwareKeyboardController.current

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
        ) {
//        Card(modifier = Modifier
//            .fillMaxWidth()
//            .padding(10.dp)
//            .height(200.dp),
//            shape = RoundedCornerShape(corner = CornerSize(16.dp)),
//            elevation = CardDefaults.cardElevation(defaultElevation = 6.dp)){
        Surface(
            modifier = modifier
                .padding(10.dp)
                .fillMaxWidth()
                .height(250.dp),
            shape = RoundedCornerShape(corner = CornerSize(8.dp)),
            border = BorderStroke(width = 1.dp, color = Color.LightGray)
        ){
            MyIOField(
                valueState = totalBillState,
                labelId = "Story",
                enabled = true,
                isSingleLine = false,
                onAction = KeyboardActions() {
//                    if(!validState){
//                        return@KeyboardActions
//                    }
                    onValChange(
                        totalBillState.value.trim()
                    )
                    Log.d("story", "MyApp: ${totalBillState.value}")
                    keyboardController?.hide()
                },
            )
        }

//        }
    }
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    TestOutlinedTextFieldTheme {
        MyApp()
    }
}