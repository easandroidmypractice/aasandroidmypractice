package com.example.forplaintext

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.joinAll
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.URL
import java.util.Locale
import kotlin.concurrent.thread

//For testing generalplaincode:
fun main() {
//Testing coroutines:
//    runBlocking {
//    //    Co Routines -> Cooperative routines
//        println("main Starts")
//        joinAll(
//    //        async is a coroutine builder that starts a new coroutine .
//            async { coroutine(1,500) },
//            async { coroutine(2,300) }
//        )
//    }
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//    runBlocking<Unit> {
//        repeat(1_000_000){
//            launch {
//                delay(500)
//                print(".")
//            }
//        }
//    }
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//    println("mainStarts")
//    threadRoutine(1,500)
//    threadRoutine(2,300)
//    Thread.sleep(1000)
//    println("mainEnds")
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//    runBlocking{
//        println("MainStarts")
//        joinAll(
//            async { suspendingCoroutines(1,500) },
//            async { suspendingCoroutines(2,300) },
//            async {
//                repeat(5){
//                    println("some other task is happening ${Thread.currentThread().name}")
//                    delay(100)
//                }
//            }
//        )
//    }
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//    runBlocking{
//        val data = fetchDataFromAPI("https://example.com/")
//        updateUI(data)
//    }
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//extension functions:
//    val str = "hello world"
//    val capitalized = str.capitalizeFirstLetter()
//    println(capitalized) // "Hello world"
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//Higher-order Functions:
//    val list = listOf(1, 2, 3, 4, 5)
//    val squareList = applyToList(list) { it * it }
//    /* the first parameter is the list,
//    the second one (operation) is a lambda where it refers to the list.
//     */
//    println(squareList)// [1, 4, 9, 16, 25]
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//Sealed Classes:
//    val circle = Shape.Circle(5.0)
//    val rectangle = Shape.Rectangle(4.0, 6.0)
//    val square = Shape.Square(3.0)
//
//    println(calculateArea(circle)) // 78.53981633974483
//    println(calculateArea(rectangle)) // 24.0
//    println(calculateArea(square)) // 9.0
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//Generics:
//    val intList = listOf(1, 2, 3, 4, 5)
//    val maxInt = max(intList)
//    println(maxInt)// 5
//
//    val stringList = listOf("foo", "bar", "baz")
//    val maxString = max(stringList)
//    println(maxString)// "foo"
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//Type Aliases:
//    val isEven: IntPredicate = { it % 2 == 0 }
//    val isOdd: IntPredicate = { it % 2 == 1 }
//
//    println(isEven(2)) // true
//    println(isOdd(2)) // false
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//Destructuring Declarations:
//    val pair = Pair("foo", "bar")
//    val (first, second) = pair
//
//    println(first) // "foo"
//    println(second) // "bar"
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//Inline Functions:
    val result = applyToInt(5) {
        it * it
    }

    println(result) // 25
    print("GeeksforGeeks: ")
    higherfunc("A Computer Science portal for Geeks", ::print)
}

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//Testing coroutines:
// suspend function are the special function that performs some long running operations
// and can be suspended by the coroutines
// can only be called from another suspend function or a coroutines
suspend fun coroutine(number: Int , delay:Long){
    println("Coroutines $number start work ")
    delay(delay)
    println("Coroutines $number has finished")
}
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//Testing coroutines:
fun threadRoutine(number: Int , delay: Long){
    thread {
        println("Thread $number starts work")
        Thread.sleep(delay)
        println("Thread $number has finished")
    }
}
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//Testing coroutines:
// coroutines can be suspended and resumed
// while suspended they don't block any thread
// in contrasts to blocked thread ,
// other tasks can be performed while coroutines are suspended.
suspend fun suspendingCoroutines(number:Int , delay:Long){
    println("Coroutines $number Starts work on ${Thread.currentThread().name}")
    delay(delay)
    println("Coroutines $number ends on ${Thread.currentThread().name}")
}
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//Testing coroutines:
suspend fun fetchDataFromAPI(url: String): String {
    return withContext(Dispatchers.IO) {
        URL(url).readText()
    }
}

fun updateUI(data: String) {
    // Update UI with data
}
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//extension functions:
fun String.capitalizeFirstLetter(): String {
    if (this.isEmpty()) {
        return this
    }
    return this.substring(0, 1).uppercase(Locale.ROOT) + this.substring(1)
}
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//Higher-order Functions:
fun applyToList(list: List<Int>,
                operation: (Int) -> Int): List<Int> {
    return list.map { operation(it) }
}
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//Sealed Classes:
sealed class Shape {
    class Circle(val radius: Double) : Shape()
    class Rectangle(val width: Double, val height: Double) : Shape()
    class Square(val sideLength: Double) : Shape()
}

fun calculateArea(shape: Shape): Double {
    return when (shape) {
        is Shape.Circle -> Math.PI * shape.radius * shape.radius
        is Shape.Rectangle -> shape.width * shape.height
        is Shape.Square -> shape.sideLength * shape.sideLength
    }
}
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//Generics:
fun <T : Comparable<T>> max(list: List<T>): T? {
    if (list.isEmpty()) {
        return null
    }
    var max = list[0]
    for (element in list) {
        if (element > max) {
            max = element
        }
    }
    return max
}
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//Type Aliases:
typealias IntPredicate = (Int) -> Boolean
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//Inline Functions:
inline fun applyToInt(value: Int, operation: (Int) -> Int): Int {
    return operation(value)
}

inline fun higherfunc( str : String, mycall :(String)-> Unit) {
    // invokes the print() by passing the string str
    mycall(str)
}
