package com.example.test_viewpager_plus_fragments.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.test_viewpager_plus_fragments.R;

import java.text.MessageFormat;

public class ScreenSlidePageFragment extends Fragment {
    int position;

    public ScreenSlidePageFragment(int position) {
        this.position = position;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_screen_slide_page, container, false);
        TextView string = view.findViewById(R.id.textViewMsg);
        String newMsg = MessageFormat.format(string.getText().toString(), position + 1);
        string.setText(newMsg);
        return view;
    }
}