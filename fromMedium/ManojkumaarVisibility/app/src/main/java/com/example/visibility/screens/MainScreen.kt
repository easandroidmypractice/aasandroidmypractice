package com.example.visibility.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.boundsInWindow
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.visibility.R

@Preview
@Composable
fun SampleComposable() {
    var isVisible by remember { mutableStateOf(false) }
    val snackbarHostState = remember { SnackbarHostState() }
    Scaffold(
        snackbarHost = {
            SnackbarHost(hostState = snackbarHostState)
        },
    )
    { contentPadding ->
    }
    LaunchedEffect(isVisible) {
        snackbarHostState.showSnackbar(if (isVisible) "Element is visible" else "Element is hidden")
    }
    Column(
        Modifier
            .fillMaxSize()
            .padding(horizontal = 30.dp)
            .verticalScroll(rememberScrollState())
    ) {
        Spacer(modifier = Modifier.height(100.dp))
        Text("Hello", modifier = Modifier.fillMaxWidth(), fontSize = 25.sp)
        Text("Coder!", modifier = Modifier.fillMaxWidth(), fontSize = 40.sp)
        Image(
            painterResource(R.drawable.ic_launcher_background),
            contentDescription = "Sample Image",
            modifier = Modifier
                .padding(top = 30.dp)
                .clip(
                    RoundedCornerShape(10.dp)
                )
        )
        Text(
            stringResource(R.string.lorem_part1),
            modifier = Modifier
                .padding(top = 30.dp)
                .fillMaxWidth(),
            fontSize = 20.sp,
            color = Color.LightGray
        )
        Text(
            stringResource(R.string.lorem_part2),
            modifier = Modifier
                .padding(top = 30.dp)
                .fillMaxWidth(),
            fontSize = 20.sp,
            color = Color.LightGray
        )
        Text(
            stringResource(R.string.lorem_part3),
            modifier = Modifier
                .padding(top = 30.dp)
                .fillMaxWidth()
                .isElementVisible {
                    isVisible = it
                },
            fontSize = 20.sp,
        )
        Text(
            stringResource(R.string.lorem_part2),
            modifier = Modifier
                .padding(top = 30.dp)
                .fillMaxWidth(),
            fontSize = 20.sp,
            color = Color.LightGray
        )
        Text(
            stringResource(R.string.lorem_part2),
            modifier = Modifier
                .padding(top = 30.dp)
                .fillMaxWidth(),
            fontSize = 20.sp,
            color = Color.LightGray

        )
        Text(
            stringResource(R.string.lorem_part2),
            modifier = Modifier
                .padding(top = 30.dp)
                .fillMaxWidth(),
            fontSize = 20.sp,
            color = Color.LightGray
        )
        Text(
            stringResource(R.string.lorem_part2),
            modifier = Modifier
                .padding(top = 30.dp)
                .fillMaxWidth(),
            fontSize = 20.sp,
            color = Color.LightGray
        )
    }
}

/**
 * is an extension function for Modifier.
 * It allows you to attach visibility detection logic to any composable element
 */
@Composable
private fun Modifier.isElementVisible(onVisibilityChanged: (Boolean) -> Unit)
            = composed {
    val isVisible by remember { derivedStateOf { mutableStateOf(false) } }

    LaunchedEffect(isVisible.value) { onVisibilityChanged.invoke(isVisible.value) }
    this.onGloballyPositioned { layoutCoordinates ->
        isVisible.value = layoutCoordinates.parentLayoutCoordinates?.let {
            val parentBounds = it.boundsInWindow()
            val childBounds = layoutCoordinates.boundsInWindow()
            parentBounds.overlaps(childBounds)
        } ?: false
    }
}