package com.example.germanhernandezdropdownlist

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.germanhernandezdropdownlist.components.DynamicSelectTextField
import com.example.germanhernandezdropdownlist.ui.theme.GermanHernandezDropDownListTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyApp{
                MainContent()
            }
        }
    }
}

@Composable
fun MyApp(content: @Composable () -> Unit) {
    GermanHernandezDropDownListTheme {
        // A surface container using the 'background' color from the theme
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {
                    content()
        }
    }
}

@Composable
fun MainContent() {
    val fruits: List<String> = listOf("Apple", "Banana", "Strawberry")
    var selectedOption by remember {
        mutableStateOf("") }

    DynamicSelectTextField(label = "for text purposes",
        onValueChangedEvent = {
                              selectedOption = it
        },
        options = fruits,
        )
}
