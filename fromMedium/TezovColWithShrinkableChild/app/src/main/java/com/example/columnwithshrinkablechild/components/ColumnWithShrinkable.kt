package com.example.columnwithshrinkablechild.components

import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.layout.Measurable
import androidx.compose.ui.layout.MeasurePolicy
import androidx.compose.ui.layout.MeasureResult
import androidx.compose.ui.layout.MeasureScope
import androidx.compose.ui.layout.ParentDataModifier
import androidx.compose.ui.layout.Placeable
import androidx.compose.ui.platform.InspectorInfo
import androidx.compose.ui.platform.InspectorValueInfo
import androidx.compose.ui.platform.debugInspectorInfo
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.util.fastForEachIndexed
import kotlin.math.max

@Composable
fun ColumnWithShrinkable(
    modifier: Modifier = Modifier,
    content: @Composable ColumnWithShrinkableScope.() -> Unit
) {
    val measurePolicy = remember {
        columnWithShrinkableMeasurePolicy()
    }
    Layout(
        content = { ColumnWithShrinkableScopeImpl.content() },
        modifier = modifier,
        measurePolicy = measurePolicy
    )
}

interface ColumnWithShrinkableScope {
    fun Modifier.shrink(value: Float): Modifier
}

private fun columnWithShrinkableMeasurePolicy() = object : MeasurePolicy {

    // some convenient accessors to retrieve the shrink modifier value.
    val Measurable.columnWithShrinkableParentData get() = (parentData as? ColumnWithShrinkableParentData)
    val Measurable.shrink get() = columnWithShrinkableParentData?.shrink

    override fun MeasureScope.measure(
        measurables: List<Measurable>,
        constraints: Constraints
    ): MeasureResult {
        // if no child, return an empty layout.
        if (measurables.isEmpty()) {
            return layout(
                constraints.minWidth,
                constraints.minHeight
            ) {}
        }
        // this will be our layout width and height.
        var boxWidth = 0
        var boxHeight = 0

        // array of all children that we will lay out in second phase.
        val placeables = arrayOfNulls<Placeable>(measurables.size)

        measurables.fastForEachIndexed { index, measurable ->
            // preferred height of the child with the max width available.
            val height = measurable.minIntrinsicHeight(constraints.maxWidth)
            // use of our modifier to alter the preferred height of the child.
            val shrink = measurable.shrink
            val measureConstraints = shrink?.let { ratio ->
                constraints.copy(
                    maxHeight = (height * ratio).toInt(),
                )
            } ?: constraints

            // use of our modifier to alter the preferred height of the child.
            val placeable = measurable.measure(measureConstraints)
            placeables[index] = placeable
            // compute the width and height of our layout with all children.
            boxWidth = max(boxWidth, placeable.width)
            boxHeight += placeable.height
        }
        // Our layout's preferred width and height are coerced to its own constraints.
        boxWidth = boxWidth.coerceIn(constraints.minWidth, constraints.maxWidth)
        boxHeight = boxHeight.coerceIn(constraints.minHeight, constraints.maxHeight)

        return layout(boxWidth, boxHeight) {
            var yOffset = 0
            placeables.forEach { placeable ->
                placeable as Placeable
                placeable.place(IntOffset(0,yOffset))
                yOffset += placeable.height
            }
        }
    }

}

//this is the interface implementation.
private object ColumnWithShrinkableScopeImpl : ColumnWithShrinkableScope {

    override fun Modifier.shrink(value: Float): Modifier {
        require(value >= 0.0f) { "invalid heightFactor $value; must be greater or equal than zero" }
        require(value <= 1.0f) { "invalid heightFactor $value; must be lesser or equal than zero" }
        return this.then(
            ModifierShrinkImpl(
                value = value,
                inspectorInfo = debugInspectorInfo {
                    name = "shrink"
                    this.value = value
                    properties["value"] = value
                }
            )
        )
    }
}
// First we need to declare a data class which will store the values
// of our modifiers. In our case, we have only one modifier.
private data class ColumnWithShrinkableParentData(
    var shrink: Float? = null,
)
// Then the srink modifier receiver.
private class ModifierShrinkImpl(
    val value: Float,
    inspectorInfo: InspectorInfo.() -> Unit
) : ParentDataModifier, InspectorValueInfo(inspectorInfo) {

    override fun Density.modifyParentData(parentData: Any?) =
        ((parentData as? ColumnWithShrinkableParentData) ?: ColumnWithShrinkableParentData()).also {
            it.shrink = value
        }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is ModifierShrinkImpl) return false
        return value == other.value
    }

    override fun hashCode(): Int {
        var result = value.hashCode()
        result = 31 * result + value.hashCode()
        return result
    }

    override fun toString(): String =
        "ModifierShrinkImpl(value=$value)"
}