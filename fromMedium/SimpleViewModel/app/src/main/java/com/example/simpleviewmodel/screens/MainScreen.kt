package com.example.simpleviewmodel.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import com.example.simpleviewmodel.models.MyViewModel

@Composable
fun MyScreen(viewModel: MyViewModel) {
    val text by viewModel.text.observeAsState()

    Column {
        Text(text = text ?: "Loading...")

        Button(onClick = { viewModel.updateText("New Text") }) {
            Text("Update Text")//this is the button label.
        }
    }
}