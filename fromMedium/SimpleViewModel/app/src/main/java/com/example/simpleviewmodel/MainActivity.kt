package com.example.simpleviewmodel
/*
this app is taken from: Building a Jetpack Compose View with ViewModel
by Rafael Meneghelo
Medium: apr 3/2023.
 */
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.example.simpleviewmodel.models.MyViewModel
import com.example.simpleviewmodel.screens.MyScreen
import com.example.simpleviewmodel.ui.theme.SimpleViewModelTheme


class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyApp{
                MainContent()
            }
        }
    }
}

@Composable
fun MyApp(content: @Composable () -> Unit) {
    SimpleViewModelTheme {
        // A surface container using the 'background' color from the theme
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {
            content()
        }
    }
}

@Composable
fun MainContent() {
    val myViewModel: MyViewModel = MyViewModel()
    MyScreen(viewModel = myViewModel)
}