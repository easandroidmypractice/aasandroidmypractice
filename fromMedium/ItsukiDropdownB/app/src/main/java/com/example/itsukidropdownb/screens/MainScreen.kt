package com.example.itsukidropdownb.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.example.itsukidropdownb.components.DropdownList
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue

@Composable
fun ContentView() {
    val itemList = listOf<String>("Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6")
    var selectedIndex by rememberSaveable { mutableStateOf(0) }

    var buttonModifier = Modifier.width(100.dp)

    Column(
        modifier = Modifier,
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        // drop down list
        DropdownList(
            itemList = itemList,
            selectedIndex = selectedIndex,
            modifier = buttonModifier,
            onItemClick = {selectedIndex = it})

//        // some other contents below the selection button and under the list
//        Text(text = "You have chosen ${itemList[selectedIndex]}",
//            textAlign = TextAlign.Center,
//            modifier = Modifier
//                .padding(3.dp)
//                .fillMaxWidth()
//                .background(Color.LightGray),)
    }
}