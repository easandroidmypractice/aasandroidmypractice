package com.example.buttons

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.buttons.screens.BasicButtonExample
import com.example.buttons.screens.ButtonWithBadgeExample
import com.example.buttons.screens.CapsuleButtonExample
import com.example.buttons.screens.CheckboxButtonExample
import com.example.buttons.screens.CircleButtonExample
import com.example.buttons.screens.FloatingActionButtonExample
import com.example.buttons.screens.GradientButtonExample
import com.example.buttons.screens.IconButtonWithTextExample
import com.example.buttons.screens.ImageButtonExample
import com.example.buttons.screens.OvalButtonExample
import com.example.buttons.screens.RadioButtonExample
import com.example.buttons.screens.RectangleButtonExample
import com.example.buttons.screens.SwitchButtonExample
import com.example.buttons.screens.ToggleButtonWithIconExample
import com.example.buttons.ui.theme.ButtonsTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyApp{
                MainContent()
            }
        }
    }
}

@Composable
fun MyApp(content: @Composable () -> Unit) {
    ButtonsTheme {
        // A surface container using the 'background' color from the theme
        Surface(
//            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {
            content()
        }
    }
}

@Preview
@Composable
fun MainContent() {
//    BasicButtonExample()
//    CircleButtonExample()
//    RectangleButtonExample()
//    CapsuleButtonExample()
//    OvalButtonExample()
//    IconButtonWithTextExample()
//    FloatingActionButtonExample()
//    ToggleButtonWithIconExample(isPlaying = true,
//                                onTogglePlay = {})
//    GradientButtonExample()
//    ButtonWithBadgeExample()
//    ImageButtonExample(onClick = {})
//    SwitchButtonExample()
//    RadioButtonExample()
    CheckboxButtonExample()
}