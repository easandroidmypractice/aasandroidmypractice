package com.example.buttons.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.material3.Badge
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Checkbox
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.RadioButton
import androidx.compose.material3.Switch
import androidx.compose.material3.SwitchDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.buttons.R

@Composable
fun BasicButtonExample() {
    Button(
        onClick = { /* Handle button click */ },
        modifier = Modifier
            .padding(16.dp)
            .fillMaxWidth()
            .height(60.dp),
        shape = RoundedCornerShape(8.dp),
        enabled = true,
        contentPadding = PaddingValues(12.dp)
    )
    {
        Text(text = "Click Me")
    }
}

@Composable
fun CircleButtonExample() {
    Button(
        onClick = { /* Handle click */ },
        shape = CircleShape,
        modifier = Modifier
            .size(70.dp)
            .padding(8.dp)
    ) {
        Text("C".uppercase(), fontWeight = FontWeight.Bold, color = Color.White)
    }
}

@Composable
fun RectangleButtonExample() {
    Button(
        onClick = { /* Handle click */ },
        shape = RectangleShape,
        modifier = Modifier
            .width(150.dp)
            .height(50.dp)
            .padding(8.dp)
    ) {
        Text("Rectangle", color = Color.White)
    }
}

@Composable
fun CapsuleButtonExample() {
    Button(
        onClick = { /* Handle click */ },
        shape = RoundedCornerShape(50),
        modifier = Modifier
            .wrapContentWidth()
            .wrapContentHeight()
            .padding(8.dp)
    ) {
        Text("Capsule", color = Color.White)
    }
}

@Composable
fun OvalButtonExample() {
    Button(
        onClick = { /* Handle click */ },
        shape = RoundedCornerShape(50.dp, 20.dp, 50.dp, 20.dp),
        modifier = Modifier
            .width(120.dp)
            .height(60.dp)
            .padding(8.dp)
    ) {
        Text("Oval", color = Color.White)
    }
}

@Composable
fun IconButtonWithTextExample() {
    Button(
        onClick = { /* Handle click */ },
        modifier = Modifier
            .padding(8.dp)
            .fillMaxWidth(),
        colors = ButtonDefaults.buttonColors(containerColor = Color.Blue)
    ) {
        Icon(
            imageVector = Icons.Default.Favorite,
            contentDescription = "Icon",
            tint = Color.White,
            modifier = Modifier.padding(end = 8.dp)
        )
        Text(text = "Like", color = Color.White)
    }
}

@Composable
fun FloatingActionButtonExample() {
    FloatingActionButton(
        onClick = { /* Handle click */ },
        modifier = Modifier.padding(16.dp),
        containerColor = MaterialTheme.colorScheme.primary,
        contentColor = Color.White
    ) {
        Icon(imageVector = Icons.Default.Favorite, contentDescription = "FAB Icon")
    }
}

@Composable
fun ToggleButtonWithIconExample(isPlaying: Boolean, onTogglePlay: () -> Unit) {
    Button(
        onClick = { onTogglePlay() },
        modifier = Modifier.padding(8.dp)
    ) {
        Icon(
            imageVector = if (isPlaying) Icons.Default.Favorite else Icons.Default.PlayArrow,
            contentDescription = if (isPlaying) "Playing" else "Paused"
        )
        Spacer(modifier = Modifier.width(8.dp))
        Text(if (isPlaying) "Playing" else "Paused")
    }
}

@Composable
fun GradientButtonExample() {
    Button(
        onClick = { /* Handle click */ },
        modifier = Modifier.padding(8.dp),
        colors = ButtonDefaults.buttonColors(containerColor = Color.LightGray)
    ) {
        Text(text = "Gradient Button", color = Color.White)
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ButtonWithBadgeExample() {
    Box(modifier = Modifier.padding(8.dp)) {
        Button(onClick = { /* Handle click */ }) {
            Text("Messages")
        }
        Badge(
            modifier = Modifier
                .align(Alignment.TopEnd)
                .offset(x = (-4).dp, y = 4.dp)
        ) {
            Text("3")
        }
    }
}

@Composable
fun ImageButtonExample(onClick: () -> Unit) {
    val image: Painter = painterResource(id = R.drawable.ic_launcher_background)
    Image(
        painter = image,
        contentDescription = "Image Button",
        contentScale = ContentScale.Crop,
        modifier = Modifier
            .size(30.dp)
            .clickable(onClick = onClick)
    )
}

@Composable
fun SwitchButtonExample() {
    var isChecked by remember { mutableStateOf(true) }

    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.spacedBy(8.dp)
    ) {
        Text(
            text = "Switch is ${if (isChecked) "On" else "Off"}",
            color = Color.White)
        Switch(
            checked = isChecked,
            onCheckedChange = { isChecked = it },
            colors = SwitchDefaults.colors(
                checkedThumbColor = Color.Green,
                uncheckedThumbColor = Color.Red
            )
        )
    }
}

@Composable
fun RadioButtonExample() {
    var selectedOption by remember { mutableStateOf("Option 1") }

    Column {
        Text(text = "Choose an option:", fontSize = 16.sp)
        listOf("Option 1", "Option 2", "Option 3").forEach { option ->
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.clickable { selectedOption = option }
            ) {
                RadioButton(
                    selected = (selectedOption == option),
                    onClick = { selectedOption = option }
                )
                Text(text = option)
            }
        }
    }
}

@Composable
fun CheckboxButtonExample() {
    var checkedState by remember { mutableStateOf(false) }

    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.spacedBy(8.dp)
    ) {
        Checkbox(
            checked = checkedState,
            onCheckedChange = { checkedState = it }
        )
        Text(text = if (checkedState) "Checked" else "Unchecked")
    }
}