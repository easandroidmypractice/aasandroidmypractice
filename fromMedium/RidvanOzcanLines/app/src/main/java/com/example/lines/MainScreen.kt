package com.example.lines

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Text
import androidx.compose.material3.VerticalDivider
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.PathEffect
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.unit.dp

@Composable
fun BasicLine(modifier: Modifier = Modifier) {
    Column {
        HorizontalDivider(thickness = 10.dp, color = Color.Blue)
        VerticalDivider(thickness = 10.dp,
            color = Color.Blue,
            modifier = Modifier.padding(start = 30.dp, bottom = 50.dp)
        )
        Canvas(
            Modifier
                .fillMaxWidth()
                .padding(bottom = 20.dp)) {
            drawLine(
                color = Color.Blue,
                start = Offset(0f, 0f),
                end = Offset(size.width, 0f),
                strokeWidth = 25f
            )
        }
    }
}

@Composable
fun StrokeCapRounded(modifier: Modifier = Modifier) {
    Canvas(
        modifier
            .fillMaxWidth()
            .padding(horizontal = 20.dp)) {
        drawLine(
            color = Color.Red,
            start = Offset(0f, 0f),
            end = Offset(size.width, 0f),
            cap = StrokeCap.Round,
            strokeWidth = 55f
        )
    }
}

@Composable
fun StrokeCapButt(modifier: Modifier = Modifier) {
    Canvas(
        modifier
            .fillMaxWidth()
            .padding(horizontal = 20.dp)) {
        drawLine(
            color = Color.Red,
            start = Offset(0f, 0f),
            end = Offset(size.width, 0f),
            cap = StrokeCap.Butt,
            strokeWidth = 55f
        )
    }
}

@Composable
fun StrokeCapSquare(modifier: Modifier = Modifier) {
    Canvas(
        modifier
            .fillMaxWidth()
            .padding(horizontal = 20.dp)) {
        drawLine(
            color = Color.Red,
            start = Offset(0f, 0f),
            end = Offset(size.width, 0f),
            cap = StrokeCap.Square,
            strokeWidth = 55f
        )
    }
}

@Composable
fun DashedLine(modifier: Modifier = Modifier) {
    val pathEffect = PathEffect.dashPathEffect(floatArrayOf(30f, 15f), 0f)
    Canvas(
        Modifier.fillMaxWidth()
    ) {
        drawLine(
            color = Color.DarkGray,
            strokeWidth = 25f,
            start = Offset(20f, 0f),
            end = Offset(size.width - 20, 0f),
            pathEffect = pathEffect
        )
    }
}

@Composable
fun CircleDashedLine(modifier: Modifier = Modifier) {
    Canvas(
        Modifier.fillMaxWidth()
    ) {
        drawLine(
            color = Color.Black,
            start = Offset(20f, 0f),
            end = Offset(size.width - 20, 0f),
            strokeWidth = 15.dp.toPx(),
            cap = StrokeCap.Round,
            pathEffect = PathEffect.dashPathEffect(
                intervals = floatArrayOf(
                    0f,
                    20.dp.toPx(),
                ),
            ),
        )
    }
}

@Composable
fun DashedBorder(modifier: Modifier = Modifier) {
    Column(
        modifier = modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center
    ) {
        val stroke = Stroke(
            width = 14f,
            pathEffect = PathEffect.dashPathEffect(floatArrayOf(20f, 20f), 0f)
        )

        Box(
            modifier = Modifier.fillMaxWidth().padding(16.dp)
                .drawBehind {
                    drawRoundRect(
                        color = Color.Blue,
                        style = stroke,
                        cornerRadius = CornerRadius(16.dp.toPx())
                    )
                }
                .clip(shape = RoundedCornerShape(16.dp))
        ) {
            Row(
                modifier = Modifier.fillMaxWidth().padding(16.dp),
                horizontalArrangement = Arrangement.Center
            ) {
                Text(
                    text = "Dashed Border"
                )
            }
        }
    }
}