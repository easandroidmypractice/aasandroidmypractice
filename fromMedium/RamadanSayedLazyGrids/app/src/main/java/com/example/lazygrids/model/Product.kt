package com.example.lazygrids.model

data class Product(
    val id: Int,
    val imageUrl: String,
    val name: String,
    val price: Double,
    val description: String
)

val productList = listOf(
    Product
        (
        1,
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1jl_IhNcfipvMyNeo3nqLEWtYTi4V8EqmxgijwFXZd0_MPv1m95PZzB9-5K1IoLpARU0&usqp=CAU",
        "Product 1",
        100.00,
        "Product 1"
    ),
    Product
        (
        2,
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1jl_IhNcfipvMyNeo3nqLEWtYTi4V8EqmxgijwFXZd0_MPv1m95PZzB9-5K1IoLpARU0&usqp=CAU",
        "Product 2",
        200.00,
        "Product 2"
    ),
    Product
        (
        3,
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1jl_IhNcfipvMyNeo3nqLEWtYTi4V8EqmxgijwFXZd0_MPv1m95PZzB9-5K1IoLpARU0&usqp=CAU",
        "Product 3",
        300.00,
        "Product 3"
    ),
    Product
        (
        4,
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1jl_IhNcfipvMyNeo3nqLEWtYTi4V8EqmxgijwFXZd0_MPv1m95PZzB9-5K1IoLpARU0&usqp=CAU",
        "Product 4",
        400.00,
        "Product 4"
    ),
    Product
        (
        5,
        "https://images-na.ssl-images-amazon.com/images/M/MV5BMjEyOTYyMzUxNl5BMl5BanBnXkFtZTcwNTg0MTUzNA@@._V1_SX1500_CR0,0,1500,999_AL_.jpg",
        "Product 5",
        500.00,
        "Product 5"
    )
)