package com.example.lazygrids

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.lazygrids.model.productList
import com.example.lazygrids.screens.AdaptiveMoviePosterCarousel
import com.example.lazygrids.screens.AdaptivePhotoGallery
import com.example.lazygrids.screens.AdaptiveStaggeredImageGallery
import com.example.lazygrids.screens.BlogFeed
import com.example.lazygrids.screens.DynamicHorizontalMoviePosterCarousel
import com.example.lazygrids.screens.DynamicVerticalGrid
import com.example.lazygrids.screens.DynamicVerticalGrid2
import com.example.lazygrids.screens.FixedProductCatalog
import com.example.lazygrids.screens.HorizontalMoviePosterCarousel
import com.example.lazygrids.screens.MoviePosterCarousel
import com.example.lazygrids.screens.VerticalGridWithHorizontalSections
import com.example.lazygrids.ui.theme.LazyGridsTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContent {
            MyApp{
                MainContent()
            }
        }
    }
}

@Composable
fun MyApp(content: @Composable () -> Unit, ) {
    LazyGridsTheme {
        content()
    }
}

@Composable
fun MainContent(modifier: Modifier = Modifier) {
    Scaffold() { innerPadding ->
        Column(
            modifier = Modifier
                .padding(innerPadding),
            verticalArrangement = Arrangement.spacedBy(16.dp),
        )
        {

//            AdaptivePhotoGallery()
//            FixedProductCatalog()
//            DynamicVerticalGrid()
//            DynamicVerticalGrid2(productList = productList)
//            MoviePosterCarousel()
//            AdaptiveMoviePosterCarousel()
//            DynamicHorizontalMoviePosterCarousel()
//            HorizontalMoviePosterCarousel()
//            VerticalGridWithHorizontalSections()
//            BlogFeed()
            AdaptiveStaggeredImageGallery()

        }
    }
}