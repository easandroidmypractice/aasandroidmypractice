package com.example.lazygrids.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.GridItemSpan
import androidx.compose.foundation.lazy.grid.LazyHorizontalGrid
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.lazy.staggeredgrid.LazyVerticalStaggeredGrid
import androidx.compose.foundation.lazy.staggeredgrid.StaggeredGridCells
import androidx.compose.foundation.lazy.staggeredgrid.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
//import coil3.compose.AsyncImage
//import coil3.compose.rememberAsyncImagePainter
import com.android.volley.toolbox.ImageRequest
import com.example.lazygrids.model.Photo
import com.example.lazygrids.model.photoList
import coil.compose.rememberAsyncImagePainter
import com.example.lazygrids.model.BlogPost
import com.example.lazygrids.model.Movie
import com.example.lazygrids.model.Product
import com.example.lazygrids.model.blogPostList
import com.example.lazygrids.model.movieList
import com.example.lazygrids.model.productList
import kotlin.math.max

@Composable
fun AdaptivePhotoGallery() {
    LazyVerticalGrid(
        columns = GridCells.Adaptive(minSize = 128.dp),
        contentPadding = PaddingValues(8.dp),
        horizontalArrangement = Arrangement.spacedBy(8.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp),
        modifier = Modifier
            .height(700.dp)  // Adjust height for better scroll experience
            .background(Color.White, RoundedCornerShape(12.dp))
            .padding(8.dp)
    ) {
        items(photoList, key = { it.id }) { photo ->
            PhotoItem(photo = photo)
        }
    }
}

@Composable
fun PhotoItem(photo: Photo) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(4.dp),
        shape = RoundedCornerShape(8.dp),
        elevation = CardDefaults.cardElevation(4.dp)
    ) {
        Column(
            modifier = Modifier
                .background(Color.LightGray)
                .padding(8.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Image(
                painter = rememberAsyncImagePainter(photo.url),
                contentDescription = "Movie Image",
                modifier = Modifier
                    .fillMaxWidth()
                    .aspectRatio(1f)  // Ensure consistent aspect ratio
                    .clip(RoundedCornerShape(8.dp)),
                contentScale = ContentScale.Crop,
            )
// The AsyncImage didn't work.
//            AsyncImage(
////                model = ImageRequest.Builder(LocalContext.current)
////                    .data(photo.url)
////                    .crossfade(true)
////                    .build(),
//                model = photo.url,
//                contentDescription = photo.description,
//                modifier = Modifier
//                    .fillMaxWidth()
//                    .aspectRatio(1f)  // Ensure consistent aspect ratio
//                    .clip(RoundedCornerShape(8.dp)),
//                contentScale = ContentScale.Crop
//            )
            Spacer(modifier = Modifier.height(4.dp))
            Text(text = photo.description, style = MaterialTheme.typography.bodySmall)
        }
    }
}

@Composable
fun FixedProductCatalog() {
    val columnQty: Int = 2
    LazyVerticalGrid(
        columns = GridCells.Fixed(columnQty),
        contentPadding = PaddingValues(8.dp),
        horizontalArrangement = Arrangement.spacedBy(8.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp),
        modifier = Modifier
            .height(800.dp)
            .background(Color.White, RoundedCornerShape(12.dp))
            .padding(8.dp)
    ) {
        items(productList, key = { it.id }) { product ->
            ProductItem(product = product)
        }
    }
}

@Composable
fun ProductItem(product: Product) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(4.dp),
        shape = RoundedCornerShape(8.dp),
        elevation = CardDefaults.cardElevation(4.dp)
    ) {
        Column(
            modifier = Modifier
                .background(Color.White)
                .padding(8.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Image(
                painter = rememberAsyncImagePainter(product.imageUrl),
                contentDescription = product.name,
                modifier = Modifier
                    .fillMaxWidth()
                    .aspectRatio(1f)  // Ensure consistent aspect ratio
                    .clip(RoundedCornerShape(8.dp)),
                contentScale = ContentScale.Crop,
            )
//            AsyncImage(
//                model = ImageRequest.Builder(LocalContext.current)
//                    .data(product.imageUrl)
//                    .crossfade(true)
//                    .build(),
//                contentDescription = product.name,
//                modifier = Modifier
//                    .fillMaxWidth()
//                    .aspectRatio(1f)  // Consistent aspect ratio
//                    .clip(RoundedCornerShape(8.dp)),
//                contentScale = ContentScale.Crop
//            )
            Spacer(modifier = Modifier.height(4.dp))
            Text(
                text = product.name,
                style = MaterialTheme.typography.titleSmall
            )
            Text(
                text = product.price.toString(),
                style = MaterialTheme.typography.bodySmall,
                color = Color.Magenta
            )
        }
    }
}

@Composable
fun DynamicVerticalGrid() {
    val configuration = LocalConfiguration.current
    val screenWidth = configuration.screenWidthDp.dp
    val horizontalPadding = 16.dp
    val itemSpacing = 8.dp
    val availableWidth = screenWidth - (horizontalPadding * 2)
    val minCellWidth = 150.dp

// Calculate the number of columns
    val columns = max(1, (availableWidth / (minCellWidth + itemSpacing)).toInt())
    LazyVerticalGrid(
        columns = GridCells.Fixed(columns),
        contentPadding = PaddingValues(8.dp),
        horizontalArrangement = Arrangement.spacedBy(itemSpacing),
        verticalArrangement = Arrangement.spacedBy(itemSpacing),
        modifier = Modifier
            .fillMaxWidth()
            .background(Color.White, RoundedCornerShape(12.dp))
            .padding(8.dp)
    ) {
        items(productList, key = { it.id }) { product ->
            ProductItem(product)
        }
    }
}

@Composable
fun DynamicVerticalGrid2(productList: List<Product>) {
    // Screen width, dynamic calculation for columns and grid height
    val configuration = LocalConfiguration.current
    val screenWidth = configuration.screenWidthDp.dp
    val horizontalPadding = 16.dp
    val itemSpacing = 8
    val availableWidth = screenWidth - horizontalPadding * 2 // Account for padding on both sides
    val minCellWidth = 150.dp // Minimum width for each item
    val columns = max(1, (availableWidth / (minCellWidth + itemSpacing.dp)).toInt())

// Calculate the number of rows based on the number of items and columns
    val rows = (productList.size + columns - 1) / columns
    // Calculate the height of the grid based on item height and vertical spacing
    val itemHeight = 250 //this a key value to render all items.
    val gridHeight = (rows * itemHeight) + ((rows - 1) * itemSpacing)
    LazyVerticalGrid(
        columns = GridCells.Fixed(columns),  // Dynamically calculated number of columns
        contentPadding = PaddingValues(8.dp),
        horizontalArrangement = Arrangement.spacedBy(itemSpacing.dp),
        verticalArrangement = Arrangement.spacedBy(itemSpacing.dp),
        modifier = Modifier
            .fillMaxWidth()  // Make sure the grid fills the width of the screen
            .height(gridHeight.dp)  // Set the height of the grid dynamically
            .background(Color.White, RoundedCornerShape(12.dp))
            .padding(8.dp)
    ) {
        items(productList, key = { it.id }) { product ->
            ProductItem(product = product)
        }
    }
}
//@Preview
@Composable
fun MoviePosterCarousel() {
    LazyHorizontalGrid(
        rows = GridCells.Fixed(2),  // Two rows in the horizontal grid
        contentPadding = PaddingValues(8.dp),
        horizontalArrangement = Arrangement.spacedBy(8.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp),
        modifier = Modifier
            .height(250.dp)  // Adjust height for horizontal grid
            .background(Color.White, RoundedCornerShape(12.dp))
            .padding(8.dp)
    ) {
        items(movieList, key = { it.id }) { movie ->
            MoviePosterItem(movie = movie)
        }
    }
}

@Composable
fun MoviePosterItem(movie: Movie) {
    Card(
        modifier = Modifier
            .padding(4.dp),
        shape = RoundedCornerShape(8.dp),
        elevation = CardDefaults.cardElevation(4.dp)
    ) {
        Column(
            modifier = Modifier
                .background(Color.LightGray)
                .padding(8.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = movie.title,
                style = MaterialTheme.typography.bodySmall,
                color = Color.Red
            )
            Spacer(modifier = Modifier.height(4.dp))
            Image(
                painter = rememberAsyncImagePainter(movie.posterUrl),
                contentDescription = "Movie Image",
                modifier = Modifier
                    .fillMaxWidth()
                    .aspectRatio(1f)  // Ensure consistent aspect ratio
                    .clip(RoundedCornerShape(8.dp)),
                contentScale = ContentScale.Crop,
            )
//            AsyncImage(
//                model = ImageRequest.Builder(LocalContext.current)
//                    .data(movie.posterUrl)
//                    .crossfade(true)
//                    .build(),
//                contentDescription = movie.title,
//                modifier = Modifier
//                    .fillMaxWidth()
//                    .aspectRatio(1f)  // Consistent aspect ratio
//                    .clip(RoundedCornerShape(8.dp)),
//                contentScale = ContentScale.Crop
//            )

        }
    }
}

//@Preview
@Composable
fun AdaptiveMoviePosterCarousel() {
    LazyHorizontalGrid(
        rows = GridCells.Adaptive(minSize = 100.dp),  // Adaptive number of rows
        contentPadding = PaddingValues(8.dp),
        horizontalArrangement = Arrangement.spacedBy(8.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp),
        modifier = Modifier
            .fillMaxWidth()
            .height(220.dp)  // Adjusted height for horizontal grid
            .background(Color.White, RoundedCornerShape(12.dp))
            .padding(14.dp)
    ) {
        items(movieList, key = { it.id }) { movie ->
            MoviePosterItem2(movie = movie)
        }
    }
}

@Composable
fun MoviePosterItem2(movie: Movie) {
    Card(
        modifier = Modifier
            .width(150.dp)
            .padding(4.dp),
        shape = RoundedCornerShape(8.dp),
        elevation = CardDefaults.cardElevation(4.dp)
    ) {
        Column(
            modifier = Modifier
                .background(Color.LightGray)
                .padding(8.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Image(
                painter = rememberAsyncImagePainter(movie.posterUrl),
                contentDescription = "Movie Image",
                modifier = Modifier
                    .fillMaxWidth()
                    .aspectRatio(1f)  // Ensure consistent aspect ratio
                    .clip(RoundedCornerShape(8.dp)),
                contentScale = ContentScale.Crop,
            )
//            AsyncImage(
//                model = ImageRequest.Builder(LocalContext.current)
//                    .data(movie.posterUrl)
//                    .crossfade(true)
//                    .build(),
//                contentDescription = movie.title,
//                modifier = Modifier
//                    .fillMaxWidth()
//                    .aspectRatio(1f)  // Ensuring consistent aspect ratio
//                    .clip(RoundedCornerShape(8.dp)),
//                contentScale = ContentScale.Crop
//            )
            Spacer(modifier = Modifier.height(4.dp))
            Text(
                text = movie.title,
                style = MaterialTheme.typography.bodySmall,
                color = Color.Red
            )
        }
    }
}

//@Preview
@Composable
fun DynamicHorizontalMoviePosterCarousel() {
    val minItemHeight = 150.dp
    val itemSpacing = 4.dp
    val configuration = LocalConfiguration.current
    val screenHeight = configuration.screenHeightDp.dp
    val availableHeight = screenHeight - (itemSpacing * 2)
    val rows = max(1, (availableHeight / (minItemHeight + itemSpacing)).toInt())
    val gridHeight = (minItemHeight * rows) + (itemSpacing * (rows - 1))
    LazyHorizontalGrid(
        rows = GridCells.Fixed(rows),
        contentPadding = PaddingValues(8.dp),
        horizontalArrangement = Arrangement.spacedBy(8.dp),
        verticalArrangement = Arrangement.spacedBy(itemSpacing),
        modifier = Modifier
            .fillMaxWidth()
            .height(gridHeight)
            .background(Color.White, RoundedCornerShape(12.dp))
            .padding(8.dp)
    ) {
        items(movieList, key = { it.id }) { movie ->
            MoviePosterItem(movie)
        }
    }
}

//@Preview
@Composable
fun HorizontalMoviePosterCarousel() {
    // Set minimum item height and spacing
    val itemHeight = 240  // Define the height of each item
    val itemSpacing = 4   // Define the spacing between items

// Number of rows in the grid (fixed in this case)
    val rows = 2  // We are fixing the number of rows to 2
    // Calculate the total grid height based on the number of rows and item spacing
    val gridHeight = (rows * itemHeight) + ((rows - 1) * itemSpacing)
    LazyHorizontalGrid(
        rows = GridCells.Fixed(rows),  // Fixed number of rows
        contentPadding = PaddingValues(8.dp),
        horizontalArrangement = Arrangement.spacedBy(8.dp),
        verticalArrangement = Arrangement.spacedBy(itemSpacing.dp),
        modifier = Modifier
            .fillMaxWidth()  // Make sure the grid fills the width of the screen
            .height(gridHeight.dp)  // Set the height of the grid dynamically
            .background(Color.White, RoundedCornerShape(12.dp))
            .padding(8.dp)
    ) {
        items(movieList, key = { it.id }) { movie ->
            MoviePosterItem3(movie = movie)
        }
    }
}

@Composable
fun MoviePosterItem3(movie: Movie) {
    Card(
        modifier = Modifier
            .width(200.dp)  // Set the width of each poster item
            .padding(4.dp),  // Add some padding around the card
        shape = RoundedCornerShape(8.dp),  // Round the corners
        elevation = CardDefaults.cardElevation(4.dp)  // Add elevation for shadow effect
    ) {
        Column(
            modifier = Modifier
                .background(Color.LightGray)
                .padding(8.dp),
            horizontalAlignment = Alignment.CenterHorizontally  // Center content horizontally
        ) {
//            AsyncImage(
//                model = ImageRequest.Builder(LocalContext.current)
//                    .data(movie.posterUrl)
//                    .crossfade(true)
//                    .build(),
//                contentDescription = movie.title,
//                modifier = Modifier
//                    .fillMaxWidth()  // Fill the width of the column
//                    .aspectRatio(1f)  // Maintain a 1:1 aspect ratio for the image
//                    .clip(RoundedCornerShape(8.dp)),  // Clip the image with rounded corners
//                contentScale = ContentScale.Crop  // Crop the image to fit
//            )
            Text(
                text = movie.title,
                style = MaterialTheme.typography.bodySmall,
                color = Color.Red  // Use white text color for contrast
            )
            Spacer(modifier = Modifier.height(10.dp))
            Image(
                painter = rememberAsyncImagePainter(movie.posterUrl),
                contentDescription = "Movie Image",
                modifier = Modifier
                    .fillMaxWidth()
                    .aspectRatio(1f)  // Ensure consistent aspect ratio
                    .clip(RoundedCornerShape(8.dp)),
                contentScale = ContentScale.Crop,
            )
        }
    }
}

@Composable
fun VerticalGridWithHorizontalSections() {
    LazyVerticalGrid(
        columns = GridCells.Fixed(2),  // Two columns in vertical grid
        contentPadding = PaddingValues(8.dp),
        horizontalArrangement = Arrangement.spacedBy(8.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp),
        modifier = Modifier
            .fillMaxWidth()
            .height(400.dp)
            .background(Color.White, RoundedCornerShape(12.dp))
            .padding(8.dp)
    ) {
        items(productList, key = { it.id }) { product ->
            ProductItem(product = product)
        }

// Adding a horizontal section in the vertical grid
        item(span = { GridItemSpan(maxLineSpan) }) {  // Span across all columns
            Text(
                text = "Featured Movies",
                style = MaterialTheme.typography.titleMedium,
                modifier = Modifier.padding(8.dp)
            )
            HorizontalMoviePosterCarousel()
        }
        item(span = { GridItemSpan(maxLineSpan) }) {  // Another horizontal section
            Text(
                text = "Top Products",
                style = MaterialTheme.typography.titleMedium,
                modifier = Modifier.padding(8.dp)
            )
//            HorizontalProductSlider()
            FixedProductCatalog()
        }
    }
}

//@Preview
@Composable
fun BlogFeed() {
    LazyVerticalStaggeredGrid(
        columns = StaggeredGridCells.Fixed(2),
        verticalItemSpacing = 8.dp,
        horizontalArrangement = Arrangement.spacedBy(2.dp),
        modifier = Modifier
            .height(400.dp)  // Adjusted height for better scrolling
            .background(Color.White, RoundedCornerShape(12.dp))
            .padding(8.dp)
    ) {
        items(blogPostList, key = { it.id }) { blogPost ->
            StaggeredBlogPostItem(blogPost = blogPost)
        }
    }
}

@Composable
fun StaggeredBlogPostItem(blogPost: BlogPost) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .height(randomHeight())  // Dynamic height for staggered effect
            .padding(4.dp),
        shape = RoundedCornerShape(8.dp),
        elevation = CardDefaults.cardElevation(4.dp)
    ) {
        Column(
            modifier = Modifier
                .background(Color.Gray)
                .padding(2.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
//            AsyncImage(
//                model = ImageRequest.Builder(LocalContext.current)
//                    .data(blogPost.imageUrl)
//                    .crossfade(true)
//                    .build(),
//                contentDescription = blogPost.title,
//                modifier = Modifier
//                    .fillMaxWidth()
//                    .clip(RoundedCornerShape(8.dp)),
//                contentScale = ContentScale.Crop
//            )
            Image(
                painter = rememberAsyncImagePainter(blogPost.imageUrl),
                contentDescription = "Movie Image",
                modifier = Modifier
                    .fillMaxWidth()
                    .aspectRatio(1f)  // Ensure consistent aspect ratio
                    .clip(RoundedCornerShape(8.dp)),
                contentScale = ContentScale.Crop,
            )
            Spacer(modifier = Modifier.height(2.dp))
            Text(text = blogPost.title, style = MaterialTheme.typography.titleSmall)
            Text(text = blogPost.previewText, style = MaterialTheme.typography.bodySmall)
        }
    }
}
fun randomHeight(): Dp {
//    return (150..300).random().dp
    return (120..200).random().dp
}

@Composable
fun AdaptiveStaggeredImageGallery() {
    LazyVerticalStaggeredGrid(
        columns = StaggeredGridCells.Adaptive(minSize = 150.dp),  // Adaptive number of columns
        verticalItemSpacing = 8.dp,
        horizontalArrangement = Arrangement.spacedBy(8.dp),
        modifier = Modifier
            .fillMaxWidth()
            .height(400.dp)  // Adjusted height for better scrolling
            .background(Color.White, RoundedCornerShape(12.dp))
            .padding(8.dp)
    ) {
        items(photoList, key = { it.id }) { photo ->
            StaggeredImageItem(photo = photo)
        }
    }
}

@Composable
fun StaggeredImageItem(photo: Photo) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .height(randomHeight())  // Dynamic height for staggered effect
            .padding(4.dp),
        shape = RoundedCornerShape(8.dp),
        elevation = CardDefaults.cardElevation(4.dp)
    ) {
        Column(
            modifier = Modifier
                .background(Color.LightGray)
                .padding(8.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
//            AsyncImage(
//                model = ImageRequest.Builder(LocalContext.current)
//                    .data(photo.url)
//                    .crossfade(true)
//                    .build(),
//                contentDescription = photo.description,
//                modifier = Modifier
//                    .fillMaxWidth()
//                    .clip(RoundedCornerShape(8.dp)),
//                contentScale = ContentScale.Crop
//            )
            Image(
                painter = rememberAsyncImagePainter(photo.url),
                contentDescription = "Movie Image",
                modifier = Modifier
                    .fillMaxWidth()
                    .aspectRatio(1f)  // Ensure consistent aspect ratio
                    .clip(RoundedCornerShape(8.dp)),
                contentScale = ContentScale.Crop,
            )
            Spacer(modifier = Modifier.height(4.dp))
            Text(text = photo.description, style = MaterialTheme.typography.bodySmall)
        }
    }
}
