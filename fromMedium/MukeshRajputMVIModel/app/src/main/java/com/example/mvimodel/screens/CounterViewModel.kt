package com.example.mvimodel.screens

import androidx.lifecycle.ViewModel
import com.example.mvimodel.models.CounterIntent
import com.example.mvimodel.models.CounterState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class CounterViewModel : ViewModel() {

    private val _state = MutableStateFlow(CounterState())
    val state: StateFlow<CounterState> get() = _state

    fun handleIntent(intent: CounterIntent) {
        when (intent) {
            is CounterIntent.Increment -> updateState { copy(count = count + 1) }
            is CounterIntent.Decrement -> updateState { copy(count = count - 1) }
        }
    }


    private fun updateState(reduce: CounterState.() -> CounterState) {
        _state.value = _state.value.reduce()
    }
}