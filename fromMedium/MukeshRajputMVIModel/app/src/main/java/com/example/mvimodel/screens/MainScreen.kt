package com.example.mvimodel.screens

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.mvimodel.models.CounterIntent

@Composable
fun CounterScreen(viewModel: CounterViewModel) {
    val state by viewModel.state.collectAsState()

    Text(text = "Count: ${state.count}", fontSize = 24.sp)
    Row {
        Button(onClick = { viewModel.handleIntent(CounterIntent.Increment) }) {
            Text("Increment")
        }
        Spacer(modifier = Modifier.width(8.dp))
        Button(onClick = { viewModel.handleIntent(CounterIntent.Decrement) }) {
            Text("Decrement")
        }
    }
}