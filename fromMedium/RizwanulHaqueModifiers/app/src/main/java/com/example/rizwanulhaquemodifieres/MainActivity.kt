package com.example.rizwanulhaquemodifieres

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.rizwanulhaquemodifieres.screens.BoxWithPadding
import com.example.rizwanulhaquemodifieres.screens.CenteredText
import com.example.rizwanulhaquemodifieres.screens.ClickableText
import com.example.rizwanulhaquemodifieres.screens.CustomStyledBox
import com.example.rizwanulhaquemodifieres.screens.GreetingCard
import com.example.rizwanulhaquemodifieres.screens.ProfilePicture
import com.example.rizwanulhaquemodifieres.screens.StyledButton
import com.example.rizwanulhaquemodifieres.screens.TextWithBorder
import com.example.rizwanulhaquemodifieres.screens.WeightedRow
import com.example.rizwanulhaquemodifieres.ui.theme.RizwanulHaqueModifieresTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContent {
            MyApp{
                MainContent()
            }
        }
    }
}

@Composable
fun MyApp(content: @Composable () -> Unit,) {
    RizwanulHaqueModifieresTheme {
        content()
    }
}
@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Preview
@Composable
fun MainContent(modifier: Modifier = Modifier) {
    Scaffold() { innerPadding ->
        Column(
            modifier = Modifier
                .padding(innerPadding),
        )
        {
//            GreetingCard()
//            BoxWithPadding()
//            CenteredText()
//            TextWithBorder()
//            ClickableText()
//            WeightedRow()
//            StyledButton()
//            CustomStyledBox()
            ProfilePicture()
        }
    }
}