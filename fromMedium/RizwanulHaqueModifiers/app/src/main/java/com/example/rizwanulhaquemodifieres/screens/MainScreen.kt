package com.example.rizwanulhaquemodifieres.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.example.rizwanulhaquemodifieres.R

@Composable
fun GreetingCard() {
    Text(
        text = "Hello, Compose!",
        modifier = Modifier
            .padding(16.dp)
            .background(MaterialTheme.colorScheme.primary)
    )

    Text(
        text = "Hello, Compose!",
        modifier = Modifier
            .padding(24.dp)
            .background(MaterialTheme.colorScheme.primary)
    )

    Text(
        text = "Hello, Compose!",
        modifier = Modifier
            .padding(16.dp)//adds 16dp padding around the Text (the widget).
            .background(MaterialTheme.colorScheme.primary)
            .padding(8.dp)//adds additional padding around the text content (inside of the widget).
    )
}

@Composable
fun BoxWithPadding() {
    Box(
        modifier = Modifier
            .size(100.dp)
            .padding(16.dp)
            .background(MaterialTheme.colorScheme.secondary)
    ) {
        Text("Box with Padding")
    }
}

@Composable
fun CenteredText() {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background)
    ) {
        Text(
            text = "Centered Text",
            modifier = Modifier.align(Alignment.Center)
        )
    }
}

@Composable
fun TextWithBorder() {
    Text(
        text = "Bordered Text",
        modifier = Modifier
            .background(Color.LightGray)
            .border(2.dp, Color.Black)
            .padding(8.dp)
    )
}

@Composable
fun ClickableText() {
    Text(
        text = "Click Me",
        modifier = Modifier
            .clickable {
                // Action to perform on click
            }
            .padding(8.dp)
    )
}

@Composable
fun WeightedRow() {
    Row(
        modifier = Modifier.fillMaxWidth()
    ) {
        Text("Item 1", modifier = Modifier.weight(1f))
        Text("Item 2", modifier = Modifier.weight(2f))
    }
}

@Composable
fun StyledButton() {
    Button(
        onClick = {},
        modifier = Modifier
            .padding(16.dp)
            .fillMaxWidth()
            .background(MaterialTheme.colorScheme.primary)
            .border(2.dp, MaterialTheme.colorScheme.secondary)
    ) {
        Text("Styled Button")
    }
}

fun Modifier.roundedCornersWithBgAndPadding(color: Color) =
    this
        .background(color, shape = RoundedCornerShape(8.dp))
        .padding(8.dp)

@Composable
fun CustomStyledBox() {
    Box(
        modifier = Modifier
            .size(100.dp)
            .roundedCornersWithBgAndPadding(MaterialTheme.colorScheme.primary)
    ) {
        Text("Custom Box")
    }
}

@Composable
fun ProfilePicture(
    modifier: Modifier = Modifier
) {
    Image(
        painter = painterResource(id = R.drawable.ic_launcher_foreground),
        contentDescription = "Profile Picture",
        modifier = modifier
            .size(100.dp)
            .clip(CircleShape)
            .border(2.dp, Color.Gray, CircleShape)
    )
}