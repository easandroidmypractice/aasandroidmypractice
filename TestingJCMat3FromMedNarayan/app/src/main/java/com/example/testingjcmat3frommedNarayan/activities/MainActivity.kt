package com.example.testingjcmat3frommedNarayan.activities

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.testingjcmat3frommedNarayan.R
import com.example.testingjcmat3frommedNarayan.ui.theme.testingjcmat3frommedNarayanTheme
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Spacer
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            testingjcmat3frommedNarayanTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
//                    Greeting("Android")
                    TravelCard()
                }
            }
        }
    }
}

//@Preview(showBackground = true)
@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun TravelCard() {
    Card(
        modifier = Modifier
            .padding(10.dp)
            .shadow(
                elevation = 5.dp,
                spotColor = MaterialTheme.colorScheme.secondaryContainer,
                shape = MaterialTheme.shapes.medium
            ),
        shape = MaterialTheme.shapes.medium
    ) {
        Column(
            Modifier
                .fillMaxWidth(),
        ) {
            //code Image() is for loading drawable images
//            Image(
//                painter = painterResource(id = R.drawable.ic_travel_dummy),
//                contentDescription = null,
//                contentScale = ContentScale.Fit,
//                modifier = Modifier
//                    .padding(8.dp)
//                    .height(150.dp)
//                    .size(84.dp)
//                    .clip(MaterialTheme.shapes.medium)
//            )
            //code GlideImage() is for loading images based in a url.
            GlideImage(
                model = "https://cdn.pixabay.com/photo/2024/02/10/02/46/fitness-8564036_1280.jpg",
//                contentDescription = getString(R.id.picture_of_cat),
                contentDescription = null,
                contentScale = ContentScale.Fit,
//                modifier = Modifier.padding(padding).clickable(onClick = onClick).fillParentMaxSize(),
                modifier = Modifier
                    .padding(8.dp)
                    .height(150.dp)
                    .size(84.dp)
                    .clip(MaterialTheme.shapes.medium)
            )
            Column(
                Modifier
                    .padding(10.dp),
            ) {
                Text(text = "Orlando Arias".uppercase(),
//                    style = appTypography.labelSmall,
                    style = MaterialTheme.typography.labelSmall,
                    color = MaterialTheme.colorScheme.onSecondaryContainer,
                    modifier = Modifier.padding(2.dp)
                )
                Text( text = "System Engineer",
//                    style = appTypography.titleLarge,
                    style = MaterialTheme.typography.titleLarge,
                    maxLines = 2,
                    color = MaterialTheme.colorScheme.onTertiaryContainer,
                    modifier = Modifier.padding(2.dp)
                )
                Text(text = "oa@yahoo.com",
//                    style = appTypography.bodySmall,
                    style = MaterialTheme.typography.bodySmall,
                    maxLines = 3,
                    color = MaterialTheme.colorScheme.onTertiaryContainer,
                    modifier = Modifier.padding(2.dp)
                )
                Spacer(modifier = Modifier.height(8.dp))
            }
        }
    }

}

@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = "Hello $name!",
        modifier = modifier
    )
}

@Preview(showBackground = true)
@Composable
fun TravelCardPreview() {
    testingjcmat3frommedNarayanTheme {
        TravelCard()
    }
}

//@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    testingjcmat3frommedNarayanTheme {
        Greeting("Android")
    }
}