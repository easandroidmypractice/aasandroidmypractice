/**
 * Created on Aug 27, 2011 by Dave Smith
 * Wireless Designs, LLC
 *
 * NoParentPressImageView.java
 * ImageView that ignores press state changes from the parent
 */
package com.example.listviewissueb;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

public class NoParentPressImageView extends androidx.appcompat.widget.AppCompatImageView {

    public NoParentPressImageView(Context context) {
        this(context, null);
    }

    public NoParentPressImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setPressed(boolean pressed) {
        // If the parent is pressed, do not set to pressed.
        if (pressed && ((View) getParent()).isPressed()) {
            return;
        }
        super.setPressed(pressed);
    }
}
