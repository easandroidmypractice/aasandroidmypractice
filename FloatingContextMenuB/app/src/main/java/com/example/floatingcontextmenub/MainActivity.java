package com.example.floatingcontextmenub;

import android.graphics.Color;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.floatingcontextmenub.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;
    final String TO_LOCK = "Lock Initial";
    final String TO_UNLOCK = "Unlock Initial";
    public boolean lockSwitch = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //binding code:
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        // here you have to register a view for context menu.
        // you can register any view like listview, image view, textview, button etc.
        registerForContextMenu(binding.myTry);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Your choices");

        if(lockSwitch){
            menu.add(v.getId(), 0, Menu.NONE, TO_LOCK);
        } else {
            menu.add(v.getId(), 0, Menu.NONE, TO_UNLOCK);
        }
        lockSwitch = !lockSwitch;
    }

    // menu item select listener
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getTitle().equals(TO_LOCK)) {
            binding.initial.setEnabled(false);
        }else{
            binding.initial.setEnabled(true);
        }
        return true;
    }
}
