package com.example.working_with_packages.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.working_with_packages.R;
    /*this project plays creating packages
    and moving classes to these ones.
     */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
