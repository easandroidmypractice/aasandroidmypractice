package com.example.animation;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView rText = (TextView) findViewById(R.id.animatedText);

        //for alpha animation.
        final Animation a = AnimationUtils.loadAnimation(this, R.anim.alpha);

        //for circular animation.
//        final Animation a = AnimationUtils.loadAnimation(this, R.anim.rotate);

        //for scale animation.
//        final Animation a = AnimationUtils.loadAnimation(this, R.anim.scale);

        //for translate animation.
//        final Animation a = AnimationUtils.loadAnimation(this, R.anim.translate);
        a.reset();

        //to stop animation:
//        textview.clearAnimation();

//        LinearLayout layout = (LinearLayout) findViewById(R.id.root);
//        layout.setOnClickListener(new View.OnClickListener() {
//            @Override public void onClick(View v) {
//                rText.startAnimation(a);
//            }
//        });

        rText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rText.startAnimation(a);
            }
        });
    }
}