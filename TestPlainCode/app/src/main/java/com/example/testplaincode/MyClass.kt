package com.example.testplaincode

fun main(){
//optimizing for loops:
    // executes three times
//    repeat(3){
//        println("some-string")
//    }
    // with an index
//    repeat(3) { index ->
//        println("some-string with index $index")
//    }
//    repeat(0) {
//        println("nothing is displayed.")
//    }
    //use for without index:
//    val bakes = listOf("sausage roll", "steak bake", "festive bake")
// Print each item in the list
//    for (bakeItem in bakes) {
//        println(bakeItem)
//    }
    //use forEach:
//    val bakes = listOf("sausage roll", "steak bake", "festive bake")
//
//// Print each item in the list
//    bakes.forEach { anyName ->
//        println(anyName)
//    }
    //user forEachIndexed:
//    val bakes = listOf("sausage roll", "steak bake", "festive bake")
//
//// Print each item in the list along with its index
//    bakes.forEachIndexed { anyNameForIndex, anyName ->
//        println("$anyNameForIndex: $anyName")
//    }
    //Use map:
//    val bakes = listOf("sausage roll", "steak bake", "festive bake")
//
//// Create a new list that contains the length of each string in the original list
//    val bakesStringLength = bakes.map { bakeItem ->
//        bakeItem.length
//        println(bakeItem.length)
//    }
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
    //lambda examples:
    //with filter and map:
//    val numbers = listOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)// Filtering even numbers and then mapping them to their squares.
//    val squaredEvenNumbers = numbers.filter { it % 2 == 0 }.map { it * it }
//    println(squaredEvenNumbers) // Output: [4, 16, 36, 64, 100]
    //Lambda with higher-order function:
//    val sum = performOperation(5, 3) { a, b -> a + b }
//    val product = performOperation(5, 3) { a, b -> a * b }
//    println("Sum: $sum, Product: $product") // Output: Sum: 8, Product: 15
    //Lambda with run extension function:
//    val greeting = run {
//        val name = "John"
//        "Hello, $name!"
//    }
//    println(greeting) // Output: Hello, John!
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
    //testing nullables:
//    val a: String? = "cat"
//    print(a.length)//Compile time error
//    println(a?.length)//3
//    println(a!!.length)//3
//    val a: String? = null
//    print(a.length)//Compile time error
//    println(a?.length)//null
//    println(a!!.length)//npe
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//calculating x and y elements in the rows and columns perimeter for my puzzles.
//unfortunately the simulation only works as in a square shape.
    var row: Int = 3
    var col: Int = 3
    val total: Int = 12
    var increment: Int = 8
    var myX:Int = 2
    var myY: Int = 2
    var switch: Boolean = false

    do {
        if(switch){
            col++
            myX += 2
            switch = !switch
        }else{
            row++
            myY += 2
            switch = !switch
        }

        increment += 2
    } while (increment < total)
    println("X elemenst are: $myX")
    println("y elemenst are: $myY")
    println("with $row rows and $col columns")
}
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//lambda examples:
//Lambda with higher-order function:
fun performOperation(x: Int, y: Int, operation: (Int, Int) -> Int): Int {//trailing lambda
    return operation(x, y)
}
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//calculating x and y elements in the rows and columns perimeter for my puzzles.