package com.example.simpledropdownmenu
/*
this app has three flavors of the DropDown menu:
the common one:
    Demo_DropDownMenu()
    the exposed:
        Demo_ExposedDropdownMenuBox()
        and the cascade:
            Demo_CascadeDropdownMenu()
    they are independently working.
    but the cascade throws an exception:
        java.lang.NoSuchMethodError: No static method AnimatedContent().
 */
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.simpledropdownmenu.components.Demo_CascadeDropdownMenu
import com.example.simpledropdownmenu.components.Demo_DropDownMenu
import com.example.simpledropdownmenu.components.Demo_ExposedDropdownMenuBox
import com.example.simpledropdownmenu.ui.theme.SimpleDropDownMenuTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyApp{
                MainContent()
            }
        }
    }
}

@Composable
fun MyApp(content: @Composable () -> Unit) {
    SimpleDropDownMenuTheme {
        // A surface container using the 'background' color from the theme
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {
            content()
        }
    }
}

@Composable
fun MainContent() {
    TheExercise()
}

@Preview
@Composable
fun TheExercise() {
    Demo_DropDownMenu()
//    Demo_ExposedDropdownMenuBox()
//    Demo_CascadeDropdownMenu()
}