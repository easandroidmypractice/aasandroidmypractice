package com.example.simpledropdownmenu.components

import android.widget.Toast
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import me.saket.cascade.CascadeDropdownMenu

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun Demo_CascadeDropdownMenu() {
    val context = LocalContext.current
    var expanded by remember { mutableStateOf(false) }

    Scaffold(
        topBar = {
            TopAppBar(
                title = { },
                actions = {
                    IconButton(onClick = { expanded = !expanded }) {
                        Icon(
                            imageVector = Icons.Default.MoreVert,
                            contentDescription = "More menu"
                        )
                    }
                }
            )
        }
    )
        {
        paddingValues ->
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(paddingValues)
                    .wrapContentSize(Alignment.TopEnd)
            )
            {
                CascadeDropdownMenu(
                    expanded = expanded,
                    onDismissRequest = { expanded = false }
                )
                {
                    DropdownMenuItem(//import cascade lib.
                        text = { Text(text = "1. Item") },
                        children = {
                            androidx.compose.material3.DropdownMenuItem(//import material3.
                                text = { Text(text = "1.1. Sub-Item") },
                                onClick = {
                                    expanded = false
                                    Toast.makeText(context, "1.1. Sub-Item", Toast.LENGTH_SHORT).show()
                                }
                            )
                        }
                    )
                    DropdownMenuItem(//import cascade lib.
                        text = { Text(text = "2. Item") },
                        children = {
                            androidx.compose.material3.DropdownMenuItem(//import material3.
                                text = { Text(text = "2.1. Sub-Item") },
                                onClick = {
                                    expanded = false
                                    Toast.makeText(context, "2.1. Sub-Item", Toast.LENGTH_SHORT).show()
                                }
                            )
                            DropdownMenuItem(//import cascade lib.
                                text = { Text(text = "2.2. Sub-Item") },
                                children = {
                                    androidx.compose.material3.DropdownMenuItem(//import material3.
                                        text = { Text(text = "2.2.1. Sub-Sub-Item") },
                                        onClick = {
                                            expanded = false
                                            Toast.makeText(context, "2.2.1. Sub-Sub-Item", Toast.LENGTH_SHORT).show()
                                        }
                                    )
                                }
                            )
                        }
                    )
                }
            }
        }
}

