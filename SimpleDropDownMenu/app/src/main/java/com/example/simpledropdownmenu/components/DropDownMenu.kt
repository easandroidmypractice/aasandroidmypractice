package com.example.simpledropdownmenu.components

import android.widget.Toast
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.DpOffset
import androidx.compose.ui.unit.dp
import com.example.simpledropdownmenu.R
import com.example.simpledropdownmenu.models.CustomDropdownMenuStyles
import com.example.simpledropdownmenu.models.getIconDrawable

@Preview
@Composable
fun Demo_DropDownMenu(buttonText: String = "Select an item",
                      styles: CustomDropdownMenuStyles = CustomDropdownMenuStyles()) {
    val context = LocalContext.current
    var expanded by remember { mutableStateOf(false) }
    styles.rightIconDrawableId = R.drawable.ic_expand_more
    styles.expandedRightIconDrawable = R.drawable.ic_expand_less

    Column(
        modifier = Modifier.fillMaxWidth()
            .wrapContentSize(Alignment.TopCenter)
    ) {
//        IconButton(onClick = { expanded = !expanded }) {
//            Icon(
//                imageVector = Icons.Default.MoreVert,
//                contentDescription = "More"
//            )
//        }
        // Dropdown button that controls menu visibility
        OutlinedDropdownButton(
            buttonText = buttonText,
            leftIconDrawableId = styles.getIconDrawable(expanded, isLeftIcon = true),
            rightIconDrawableId = styles.getIconDrawable(expanded, isLeftIcon = false),
            styles = styles
        ) {
            expanded = true
        }

        DropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false },
        ) {
            DropdownMenuItem(
                text = { Text("Load") },
                onClick = { Toast.makeText(context, "Load", Toast.LENGTH_SHORT).show() }
            )
            DropdownMenuItem(
                text = { Text("Save") },
                onClick = { Toast.makeText(context, "Save", Toast.LENGTH_SHORT).show() }
            )
        }
    }
}

// Dropdown button component
@Composable
fun OutlinedDropdownButton(
    buttonText: String,
    leftIconDrawableId: Int?,
    rightIconDrawableId: Int?,
    styles: CustomDropdownMenuStyles,
    onClick: () -> Unit
) {
    OutlinedButton(
        onClick = onClick,
        modifier = Modifier
            .height(styles.height)
            .wrapContentWidth(),
        border = styles.buttonBorderStroke,
        shape = RoundedCornerShape(styles.buttonCornerRadius),
        colors = ButtonDefaults.outlinedButtonColors(
            containerColor = styles.buttonContainerColor
        ),
        contentPadding = PaddingValues(
            horizontal = styles.buttonHorizontalPadding,
            vertical = styles.buttonVerticalPadding
        )
    ) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            // Optional left icon
            leftIconDrawableId?.let {
                Icon(
                    imageVector = ImageVector.vectorResource(id = it),
                    contentDescription = null,
                    tint = styles.iconColor
                )
                Spacer(modifier = Modifier.width(4.dp))
            }
            // Button text
            Text(buttonText, style = styles.buttonTextStyle)
            // Optional right icon
            rightIconDrawableId?.let {
                Spacer(modifier = Modifier.width(4.dp))
                Icon(
                    imageVector = ImageVector.vectorResource(id = it),
                    contentDescription = null,
                    tint = styles.iconColor
                )
            }
        }
    }
}