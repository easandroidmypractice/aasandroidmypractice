package com.example.testsplashscreen.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import com.example.testsplashscreen.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private var binding: ActivityMainBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        installSplashScreen()
        setContentView(binding?.root)
//        toast(R.string.any_string)
    }

    override fun onDestroy() {
        super.onDestroy()
        //TO avoid memory leak we disallow the binding once the activity is destroyed
        binding = null
    }
}