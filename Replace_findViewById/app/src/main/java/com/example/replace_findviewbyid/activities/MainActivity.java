package com.example.replace_findviewbyid.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.replace_findviewbyid.R;
import com.example.replace_findviewbyid.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    /* this is a copy of Section_01b.
     the next object is instantiated from a class created using the activity_main layout name
    plus Binding.
     */
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.buttonMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this,
                        "button clicked using code with anonymous class",
                        Toast.LENGTH_LONG).show();
            }
        });
    }
}
