package com.example.dropdownlist

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.platform.LocalFocusManager

@Composable
fun TestingFocusFunct1() {//passes the focus after clicking the button.
    Column {
        val focusRequester = remember { FocusRequester() }
        var value by remember { mutableStateOf("") }

        TextField(
            modifier = Modifier.focusRequester(focusRequester),
            value = value,
            onValueChange = {
                value = it
            }
        )

        Button(onClick = {
            focusRequester.requestFocus()
        }) {
            Text("Gain focus")
        }
    }
}

@Composable
fun TestingFocusFunct2() {
    Column {
        val focusManager = LocalFocusManager.current
        var value by remember { mutableStateOf("") }

        TextField(
            value = value,
            onValueChange = {
                value = it
            },
            singleLine = true,
            keyboardActions = KeyboardActions() {
                focusManager.moveFocus(FocusDirection.Next)
            },
        )
        TextField(
            value = value,
            onValueChange = {
                value = it
            },
            singleLine = true,
            keyboardActions = KeyboardActions {
                focusManager.clearFocus()
            },
        )
    }
}