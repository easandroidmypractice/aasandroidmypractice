package com.example.animationc;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Button startButton;
    ImageView image;
    TextView textView1;
    TextView textView2;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        image = findViewById(R.id.image);
        startButton = findViewById(R.id.start);
        textView1 = findViewById(R.id.textView1);
        textView2 = findViewById(R.id.textView2);
        
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //startColorAnimation(image);
                startColorAnimation(textView1);
                startColorAnimation(textView2);
            }
        });
    }

    private void startColorAnimation(View v) {
        int colorStart = v.getSolidColor();
        //int colorEnd = 0xFFFF0000;
        int colorEnd = 0xaaadff2f;

        ValueAnimator colorAnim = ObjectAnimator.ofInt(v, "backgroundColor", colorStart, colorEnd);

        colorAnim.setDuration(3000);
        colorAnim.setEvaluator(new ArgbEvaluator());
        colorAnim.setRepeatCount(1);
        colorAnim.setRepeatMode(ValueAnimator.REVERSE);
        colorAnim.start();
    }
}