package com.example.animationb;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    Button startButton;
    ImageView image;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        image = findViewById(R.id.image);
        startButton = findViewById(R.id.start);
        
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startColorAnimation(image);
            }
        });
    }

    private void startColorAnimation(ImageView image) {
        int colorStart = image.getSolidColor();
        int colorEnd = 0xFFFF0000;

        ValueAnimator colorAnim = ObjectAnimator.ofInt(image, "backgroundColor", colorStart, colorEnd);

        colorAnim.setDuration(2000);
        colorAnim.setEvaluator(new ArgbEvaluator());
        colorAnim.setRepeatCount(1);
        colorAnim.setRepeatMode(ValueAnimator.REVERSE);
        colorAnim.start();
    }
}