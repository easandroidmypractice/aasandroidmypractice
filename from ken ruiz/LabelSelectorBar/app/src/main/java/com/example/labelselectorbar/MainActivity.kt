package com.example.labelselectorbar

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.labelselectorbar.components.LabelSelectorBar
import com.example.labelselectorbar.components.LabelSelectorBarPreview
import com.example.labelselectorbar.components.LabelUiPreview
import com.example.labelselectorbar.ui.theme.LabelSelectorBarTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyApp{
                MainContent()
            }
        }
    }
}

@Composable
fun MyApp(content: @Composable () -> Unit) {
    LabelSelectorBarTheme {
        // A surface container using the 'background' color from the theme
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {
            content()
        }
    }
}

@Composable
fun MainContent() {
    TheExercise()
}

@Preview
@Composable
fun TheExercise() {
//    LabelUiPreview()
//    LabelSelectorBarPreview()
    LabelSelectorBar(
        labelItems = listOf(
            "All", "Pop", "Rock", "Jazz", "Hip Hop", "Classical"
        ),
        barHeight = 80.dp,
        horizontalPadding = 12.dp,
        distanceBetweenItems = 8.dp,
        backgroundColor = Color(0xFFB2F0AD),
        selectedBackgroundColor = Color(0xFF1334E9),
        textColor = Color(0xFF333333),
        selectedTextColor = Color(0xFFD8D1D1),
        roundedCornerShapeSize = 24.dp,
        labelVerticalPadding = 14.dp,
        labelHorizontalPadding = 18.dp,
    )
}