package com.example.dropdownmenub.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Icon
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.unit.dp
import com.example.dropdownmenub.models.CustomDropdownMenuStyles
import com.example.dropdownmenub.models.getIconDrawable

// Renders a custom dropdown menu
@Composable
fun CustomDropdownMenu(
    modifier: Modifier = Modifier,
    buttonText: String = "Select an item",
    menuItems: List<Pair<String, () -> Unit>>,
    styles: CustomDropdownMenuStyles = CustomDropdownMenuStyles()
) {
    val expanded = remember { mutableStateOf(false) }

    Column(modifier = modifier) {
        // Dropdown button that controls menu visibility
        OutlinedDropdownButton(
            buttonText = buttonText,
            leftIconDrawableId = styles.getIconDrawable(expanded.value, isLeftIcon = true),
            rightIconDrawableId = styles.getIconDrawable(expanded.value, isLeftIcon = false),
            styles = styles,
        ) {
            expanded.value = true
        }
        // Conditional display of menu items
        DropdownMenu(
            expanded = expanded.value,
            onDismissRequest = { expanded.value = false }
        ) {
            menuItems.forEach { (label, onClick) ->
                DropdownMenuItem(
                    text = { Text(label, style = styles.menuItemTextStyle) },
                    onClick = {
                        expanded.value = false
                        onClick()
                    },
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(styles.menuItemHeight),
                    contentPadding = PaddingValues(horizontal = styles.menuItemHorizontalPadding)
                )
            }
        }
    }
}

// Dropdown button component
@Composable
fun OutlinedDropdownButton(
    buttonText: String,
    leftIconDrawableId: Int?,
    rightIconDrawableId: Int?,
    styles: CustomDropdownMenuStyles,
    onClick: () -> Unit
) {
    OutlinedButton(
        onClick = onClick,
        modifier = Modifier
            .height(styles.height)
            .wrapContentWidth(),
        border = styles.buttonBorderStroke,
        shape = RoundedCornerShape(styles.buttonCornerRadius),
        colors = ButtonDefaults.outlinedButtonColors(
            containerColor = styles.buttonContainerColor
        ),
        contentPadding = PaddingValues(
            horizontal = styles.buttonHorizontalPadding,
            vertical = styles.buttonVerticalPadding
        )
    ) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            // Optional left icon
            leftIconDrawableId?.let {
                Icon(
                    imageVector = ImageVector.vectorResource(id = it),
                    contentDescription = null,
                    tint = styles.iconColor
                )
                Spacer(modifier = Modifier.width(4.dp))
            }
            // Button text
            Text(buttonText, style = styles.buttonTextStyle)
            // Optional right icon
            rightIconDrawableId?.let {
                Spacer(modifier = Modifier.width(4.dp))
                Icon(
                    imageVector = ImageVector.vectorResource(id = it),
                    contentDescription = null,
                    tint = styles.iconColor
                )
            }
        }
    }
}