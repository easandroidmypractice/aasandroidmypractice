package com.example.dropdownmenub.utils

import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import com.example.dropdownmenub.R
import com.example.dropdownmenub.components.CustomDropdownMenu
import com.example.dropdownmenub.models.CustomDropdownMenuStyles

@Preview
@Composable
fun CustomDropdownMenuPreview() {
    val context = LocalContext.current
    var gameLevel by remember{
        mutableStateOf("")
    }

    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(Color(0xFFB4BACC)),
        contentAlignment = Alignment.Center
    ) {
        Column {
            CustomDropdownMenu(
                menuItems = listOf(
                    "Item 1" to {
                        Toast
                            .makeText(context, "Item 1 clicked", Toast.LENGTH_SHORT)
                            .show()
                        gameLevel = "AAA"
                        Log.d("dropdown", "you selected: $gameLevel")
                    },
                    "Item 2" to {
                        Toast
                            .makeText(context, "Item 2 clicked", Toast.LENGTH_SHORT)
                            .show()
                        gameLevel = "BBB"
                        Log.d("dropdown", "you selected: $gameLevel")
                    },
                    "Item 3" to {
                        Toast
                            .makeText(context, "Item 3 clicked", Toast.LENGTH_SHORT)
                            .show()
                        gameLevel = "CCC"
                        Log.d("dropdown", "you selected: $gameLevel")
                    }
                ),
                styles = CustomDropdownMenuStyles(
                    /* it passes 2 icons to be rendered to the right side
                    but not simultaneously.
                     */
                    rightIconDrawableId = R.drawable.ic_expand_more,
                    expandedRightIconDrawable = R.drawable.ic_expand_less
                ),
            )
//            Spacer(modifier = Modifier.height(16.dp))
//            CustomDropdownMenu(
//                buttonText = "Select an option",
//                menuItems = listOf(
//                    "Option 1" to {
//                        Toast
//                            .makeText(context, "Option 1 clicked", Toast.LENGTH_SHORT)
//                            .show()
//                    },
//                    "Option 2" to {
//                        Toast
//                            .makeText(context, "Option 2 clicked", Toast.LENGTH_SHORT)
//                            .show()
//                    },
//                    "Option 3" to {
//                        Toast
//                            .makeText(context, "Option 3 clicked", Toast.LENGTH_SHORT)
//                            .show()
//                    }
//                ),
//                styles = CustomDropdownMenuStyles(
                    /* it passes 1 icon to be rendered to the left side.
                     */
//                    leftIconDrawableId = R.drawable.ic_more
//                )
//            )
        }
    }
}