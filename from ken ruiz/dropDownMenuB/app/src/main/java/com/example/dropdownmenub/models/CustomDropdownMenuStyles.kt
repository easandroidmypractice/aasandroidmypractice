package com.example.dropdownmenub.models
import androidx.compose.foundation.BorderStroke
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

data class CustomDropdownMenuStyles(
// Styles configuration for dropdown menu
    val height: Dp = 45.dp,//changed from 40.dp because truncation.
    val mainColor: Color = Color(0xFF31394F),
    val strokeColor: Color = mainColor.copy(alpha = 0.4f),
    val buttonContainerColor: Color = Color(0xFFF8FAFC),
    val buttonCornerRadius: Dp = 12.dp,
    val buttonVerticalPadding: Dp = 12.dp,
    val buttonBorderStroke: BorderStroke = BorderStroke(1.dp, strokeColor),
    val buttonHorizontalPadding: Dp = 16.dp,
    val buttonTextStyle: TextStyle = TextStyle(
        fontWeight = FontWeight.Medium,
        fontSize = 14.sp,
        color = mainColor.copy(alpha = 0.8f)
    ),
    val menuItemTextStyle: TextStyle = TextStyle(
        fontWeight = FontWeight.Normal,
        fontSize = 14.sp,
        color = mainColor.copy(alpha = 0.8f)
    ),
    val menuItemHeight: Dp = 40.dp,
    val menuItemHorizontalPadding: Dp = 16.dp,
    val iconColor: Color = mainColor.copy(alpha = 0.8f),
    val leftIconDrawableId: Int? = null,
    val expandedLeftIconDrawable: Int? = null,
    val rightIconDrawableId: Int? = null,
    val expandedRightIconDrawable: Int? = null
)

// Chooses the correct icon based on the dropdown's state
//this is an extension function.
fun CustomDropdownMenuStyles.getIconDrawable(expanded: Boolean, isLeftIcon: Boolean): Int? {
    return if (isLeftIcon) {
        if (expanded) expandedLeftIconDrawable?: leftIconDrawableId else leftIconDrawableId
    } else {
        if (expanded) expandedRightIconDrawable?: rightIconDrawableId else rightIconDrawableId
    }
}