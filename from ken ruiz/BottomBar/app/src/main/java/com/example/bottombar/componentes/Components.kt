package com.example.bottombar.componentes

import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.example.bottombar.R
import com.example.bottombar.data.BottomBarItemData

/**
 * Composable function to create a bottom bar item for navigation.
 * BottomBarItemUi(), composable for each item in the bottom bar.
 *
 * @param itemData Data for the bottom bar item, includes icon ID and title. Default is an empty data set.
 * @param selected Indicates if the item is currently selected. Default is false.
 * @param primaryColor The primary color used for the selected state (icon bg). Default is black (0xFF000000).
 * @param secondaryColor The secondary color used for the non-selected state (icon bg). Default is white (0xFFFFFFFF).
 * @param iconSize The size of the icon. Default value is 24.dp.
 * @param selectionCircleSize The size of the selection circle shown when selected. Default is 36.dp.
 * @param modifier Modifier applied to the composable for customization. Default is an empty Modifier.
 * @param onClick Lambda function to handle click events. Receives the item data as a parameter.
 */
@Composable
fun BottomBarItemUi(
    itemData: BottomBarItemData = BottomBarItemData(),
    selected: Boolean = false,
    primaryColor: Color = Color(0xFF000000),
    secondaryColor: Color = Color(0xFFFFFFFF),
    iconSize: Dp = 24.dp,
    selectionCircleSize: Dp = 36.dp,
    modifier: Modifier = Modifier,
    // 1. Action Handler as a Trailing Lambda
    onClick: (BottomBarItemData) -> Unit = {},
) {
    // 2. Selection Indicator UX
    val color = if (selected) primaryColor else secondaryColor
    val alphaValue by animateFloatAsState(targetValue = if (selected) 1f else 0f)
    Box(
        contentAlignment = Alignment.Center,
        modifier = modifier.then(Modifier.clickable {
            onClick(itemData)//allows to click on a specific element.
        })
    ) {
        Box(
            modifier = Modifier
                .size(selectionCircleSize)
                .background(secondaryColor.copy(alpha = alphaValue), CircleShape)
        )
        Icon(
            painter = painterResource(id = itemData.iconId),
            contentDescription = itemData.title,
            tint = color,
            modifier = Modifier.size(iconSize)
        )
    }
}

/**
 * Composable function to create a customizable bottom navigation bar with multiple items.
 * BottomBar(), which is the container for BottomBarItemUi()s.
 *
 *
 * @param bottomBarItems List of data items for each bottom bar item. Default is an empty list.
 * @param selectedBottomBarItem Mutable state to track the currently selected item. Default is empty BottomBarItemData.
 * @param barHeight Height of the bottom bar. Default value is 64.dp.
 * @param primaryColor Primary color used for the bar and selected items. Default is black (0xFF000000).
 * @param secondaryColor Secondary color used for non-selected items. Default is white (0xFFFFFFFF).
 * @param iconSize Size of the icons in the bar. Default value is 24.dp.
 * @param selectionCircleSize Size of the selection circle around selected items. Default is 36.dp.
 * @param modifier Modifier applied to the composable for customization. Default is an empty Modifier.
 */
@Composable
fun BottomBar(
    bottomBarItems: List<BottomBarItemData> = listOf(),
    // 1. Centralized Selection State
    selectedBottomBarItem: MutableState<BottomBarItemData> = mutableStateOf(BottomBarItemData()),
    barHeight: Dp = 64.dp,
    primaryColor: Color = Color(0xFF000000),
    secondaryColor: Color = Color(0xFFFFFFFF),
    iconSize: Dp = 24.dp,
    selectionCircleSize: Dp = 36.dp,
    modifier: Modifier = Modifier
) {
    // 2. Item Width Calculation (total screen width / number of elements from the bottomBarItems array.
    val itemWidth = LocalConfiguration.current.screenWidthDp.dp / bottomBarItems.size
    LazyRow(
        modifier = modifier
            .fillMaxWidth()
            .height(barHeight)
            .background(primaryColor),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        items(bottomBarItems) { item ->
            BottomBarItemUi(
                itemData = item,
                modifier = Modifier.width(itemWidth),
                // 3. Ensured Single Selection Logic
                selected = selectedBottomBarItem.value.id == item.id,
                primaryColor = primaryColor,
                secondaryColor = secondaryColor,
                iconSize = iconSize,
                selectionCircleSize = selectionCircleSize
            ) {
                selectedBottomBarItem.value = item
            }
        }
    }
}

@Preview
@Composable
fun BottomBarItemUiPreview() {
    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxSize()
            .background(Color(0xFF000000))
    ) {
        Text("Selected", color = Color.White)
        Spacer(modifier = Modifier.height(8.dp))
        BottomBarItemUi(
            itemData = BottomBarItemData(1, "Home", R.drawable.ic_home),
            selected = true
        )
        Spacer(modifier = Modifier.height(16.dp))
        Text("Not Selected", color = Color.White)
        Spacer(modifier = Modifier.height(8.dp))
        BottomBarItemUi(
            itemData = BottomBarItemData(1, "Home", R.drawable.ic_home),
            selected = false
        )
    }
}

@Preview
@Composable
fun BottomBarPreview() {
    BottomBar(
        bottomBarItems = listOf(
            BottomBarItemData(1, "Home", R.drawable.ic_home),
            BottomBarItemData(2, "Search", R.drawable.ic_search),
            BottomBarItemData(3, "Library", R.drawable.ic_music_library)
        )
    )
}