package com.example.bottombar.data

data class BottomBarItemData(
    val id: Int = -1,
    val title: String = "",
    val iconId: Int = -1
)
