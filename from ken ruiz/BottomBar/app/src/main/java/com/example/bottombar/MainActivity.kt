package com.example.bottombar

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import com.example.bottombar.componentes.BottomBar
import com.example.bottombar.componentes.BottomBarItemUiPreview
import com.example.bottombar.componentes.BottomBarPreview
import com.example.bottombar.data.BottomBarItemData
import com.example.bottombar.ui.theme.BottomBarTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyApp{
                MainContent()
            }
        }
    }
}

@Composable
fun MyApp(content: @Composable () -> Unit) {
    BottomBarTheme {
        // A surface container using the 'background' color from the theme
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {
            content()
        }
    }
}

@Composable
fun MainContent() {
    TheExercise()
}

@Preview
@Composable
fun TheExercise() {
    // 1. Managing Selected Item State
    val selectedBottomBarItem =
        remember { mutableStateOf(BottomBarItemData(1, "Home", R.drawable.ic_home)) }
    Column {
        Text(
            text = "Selected: ${selectedBottomBarItem.value.title}",
//            modifier = Modifier.align(Alignment.Center),
            fontSize = 20.sp
        )
        when(selectedBottomBarItem.value.id) {
            1 -> Text(
                text = "do something with Home",
                fontSize = 20.sp
            )
            2 -> Text(
                text = "do something with Search",
                fontSize = 20.sp
            )
            3 -> Text(
                text = "do something with Library",
                fontSize = 20.sp
            )
            4 -> Text(
                text = "do something with Settings",
                fontSize = 20.sp
            )
        }
    }

    Box(modifier = Modifier.fillMaxSize()) {

        BottomBar(
            selectedBottomBarItem = selectedBottomBarItem,
            bottomBarItems = listOf(
                BottomBarItemData(1, "Home", R.drawable.ic_home),
                BottomBarItemData(2, "Search", R.drawable.ic_search),
                BottomBarItemData(3, "Library", R.drawable.ic_music_library),
                BottomBarItemData(4, "Settings", R.drawable.ic_settings)
            ),
            primaryColor = Color(0xFF3F51B5),
            secondaryColor = Color(0xFFC4CBF5),
            // 2. Bottom Bar Layout and Alignment
            modifier = Modifier
                .align(Alignment.BottomStart)
                .fillMaxWidth()
        )

    }
}

