package com.example.withgooglefonts

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.Alignment
import androidx.compose.ui.text.font.FontWeight
import com.example.withgooglefonts.ui.theme.WithGoogleFontsTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyApp{
                MainContent()
            }
        }
    }
}

@Composable
fun MyApp(content: @Composable () -> Unit) {
    WithGoogleFontsTheme {
        // A surface container using the 'background' color from the theme
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {
            content()
        }
    }
}

@Composable
fun MainContent() {
//    PreviewUserRatingBar()//initial approach.
    TheExercise()
}

@Preview
@Composable
fun TheExercise() {
    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Text(
            text = "Regular",
            fontFamily = ManropeFontFamily,
            fontWeight = FontWeight.Normal
        )
        Text(
            text = "Medium",
            fontFamily = ManropeFontFamily,
            fontWeight = FontWeight.Medium
        )
        Text(
            text = "Semi Bold",
            fontFamily = ManropeFontFamily,
            fontWeight = FontWeight.SemiBold
        )
        Text(
            text = "Bold",
            fontFamily = ManropeFontFamily,
            fontWeight = FontWeight.Bold
        )
        Text(
            text = "Extra Bold",
            fontFamily = ManropeFontFamily,
            fontWeight = FontWeight.ExtraBold
        )
        Text(
            text = "Light",
            fontFamily = ManropeFontFamily,
            fontWeight = FontWeight.Light
        )
        Text(
            text = "Extra Light",
            fontFamily = ManropeFontFamily,
            fontWeight = FontWeight.ExtraLight
        )
    }
}