package com.example.withgooglefonts

import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight

/*
Each Font object represents a specific font file along with a FontWeight.
FontWeight indicates the thickness of the glyphs in the font file.
The standard font weights are:

FontWeight.Thin or FontWeight.W100
FontWeight.ExtraLight or FontWeight.W200
FontWeight.Light or FontWeight.W300
FontWeight.Normal or FontWeight.W400
FontWeight.Medium or FontWeight.W500
FontWeight.SemiBold or FontWeight.W600
FontWeight.Bold or FontWeight.W700
FontWeight.ExtraBold or FontWeight.W800
FontWeight.Black or FontWeight.W900
 */
val ManropeFontFamily = FontFamily(
    Font(R.font.manrope_bold, FontWeight.Bold),
    Font(R.font.manrope_extra_bold, FontWeight.ExtraBold),
    Font(R.font.manrope_light, FontWeight.Light),
    Font(R.font.manrope_medium, FontWeight.Medium),
    Font(R.font.manrope_regular, FontWeight.Normal),
    Font(R.font.manrope_semi_bold, FontWeight.SemiBold),
    Font(R.font.manrope_extra_light, FontWeight.ExtraLight)
)