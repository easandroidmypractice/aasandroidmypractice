package com.example.userrating

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.userrating.components.PreviewUserRatingBar
import com.example.userrating.components.UserRatingBar
import com.example.userrating.ui.theme.UserRatingTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyApp{
                MainContent()
            }
        }
    }
}

@Composable
fun MyApp(content: @Composable () -> Unit) {
    UserRatingTheme {
        // A surface container using the 'background' color from the theme
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {
            content()
        }
    }
}

@Composable
fun MainContent() {
//    PreviewUserRatingBar()//initial approach.
    TheExercise()
}

@Preview
@Composable
fun TheExercise() {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        // 1. Rating State
        val ratingState = remember { mutableIntStateOf(0) } //mutable for Integers.
        UserRatingBar(
            // 2. Customized UserRatingBar
            ratingState = ratingState,
            ratingIconPainter = painterResource(id = R.drawable.ic_star_2),
            size = 48.dp,
            selectedColor = Color(0xFF1616E4),)

        // 3. Current Selected Value Feedback
        Text(
            text = "Current Rating Bar Value: ${ratingState.intValue}",
            fontWeight = FontWeight.Bold,
            fontSize = 20.sp
        )
    }
}