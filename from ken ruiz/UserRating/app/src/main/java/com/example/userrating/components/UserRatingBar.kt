package com.example.userrating.components

import android.view.MotionEvent
import androidx.compose.animation.animateColorAsState
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.input.pointer.pointerInteropFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.example.userrating.R

/**
 * UserRatingBar parameter list:
 *
 * @param modifier Modifier applied to the composable for customization. Default is an empty Modifier.
 * @param size sets each star icon's dimension, here defaulted to 64.dp.
 * @param ratingState it is a mutable integer state that not only tracks the current rating,
 *                    but also allows for the exposure of the selected rating value outside the composable.
 * @param ratingIconPainter uses the ic_star drawable for star rendering.
 * @param selectedColor determines the star icons' colors when they are selected.
 * @param unselectedColor determines the star icons' colors when they are unselected.
 */
@Composable
fun UserRatingBar(
    // 1. Parameters for UserRatingBar
    modifier: Modifier = Modifier,
    size: Dp = 64.dp,
    ratingState: MutableState<Int> = remember { mutableIntStateOf(0) },
    ratingIconPainter: Painter = painterResource(id = R.drawable.ic_star),
    selectedColor: Color = Color(0xFFFFD700),
    unselectedColor: Color = Color(0xFFA2ADB1)
) {
    Row(modifier = modifier.wrapContentSize()) {
        // 2. Star Icon Generation Loop
        for (value in 1..5) {
            StarIcon(
                size = size,
                painter = ratingIconPainter,
                ratingValue = value,
                ratingState = ratingState,
                selectedColor = selectedColor,
                unselectedColor = unselectedColor
            )
        }
    }
}

/**
 * UserRatingBar parameter list:
 *
 * @param size same as in UserRatingBar().
 * @param ratingState same as in UserRatingBar().
 * @param painter allows to draw.
 * @param ratingValue is specific to each star, representing its contribution to the overall rating.
 * @param selectedColor same as in UserRatingBar().
 * @param unselectedColor same as in UserRatingBar().
 */
@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun StarIcon(
    // 3. Parameters for StarIcon
    size: Dp,
    ratingState: MutableState<Int>,
    painter: Painter,
    ratingValue: Int,
    selectedColor: Color,
    unselectedColor: Color
) {
    // 4. Color Animation
    val tint by animateColorAsState(
        targetValue = if (ratingValue <= ratingState.value) selectedColor else unselectedColor,
        label = ""
    )

    Icon(
        painter = painter,
        contentDescription = null,
        modifier = Modifier
            .size(size)
            // 5. Touch Interaction Handling
            .pointerInteropFilter {
                when (it.action) {
                    MotionEvent.ACTION_DOWN -> {
                        ratingState.value = ratingValue
                    }
                }
                true
            },
        tint = tint
    )
}

@Preview
@Composable
fun PreviewUserRatingBar() {
    val ratingState = remember { mutableIntStateOf(3) }
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        UserRatingBar(ratingState = ratingState)
    }
}