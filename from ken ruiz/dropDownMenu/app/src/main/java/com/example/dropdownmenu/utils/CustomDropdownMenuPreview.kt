package com.example.dropdownmenu.utils

import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.dropdownmenu.R
import com.example.dropdownmenu.components.CustomDropdownMenu
import com.example.dropdownmenu.components.CustomDropdownMenuStyles

@Preview
@Composable
fun CustomDropdownMenuPreview() {
    val context = LocalContext.current
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(Color(0xFFB4BACC)),
        contentAlignment = Alignment.Center
    ) {
        Column {
            CustomDropdownMenu(
                menuItems = listOf(
                    "Item 1" to {
                        Toast
                            .makeText(context, "Item 1 clicked", Toast.LENGTH_SHORT)
                            .show()
                    },
                    "Item 2" to {
                        Toast
                            .makeText(context, "Item 2 clicked", Toast.LENGTH_SHORT)
                            .show()
                    },
                    "Item 3" to {
                        Toast
                            .makeText(context, "Item 3 clicked", Toast.LENGTH_SHORT)
                            .show()
                    }
                ),
                styles = CustomDropdownMenuStyles(
                    rightIconDrawableId = R.drawable.ic_expand_more,
                    expandedRightIconDrawable = R.drawable.ic_expand_less
                ),
            )
            Spacer(modifier = Modifier.height(16.dp))
            CustomDropdownMenu(
                buttonText = "Select an option",
                menuItems = listOf(
                    "Option 1" to {
                        Toast
                            .makeText(context, "Option 1 clicked", Toast.LENGTH_SHORT)
                            .show()
                    },
                    "Option 2" to {
                        Toast
                            .makeText(context, "Option 2 clicked", Toast.LENGTH_SHORT)
                            .show()
                    },
                    "Option 3" to {
                        Toast
                            .makeText(context, "Option 3 clicked", Toast.LENGTH_SHORT)
                            .show()
                    }
                ),
                styles = CustomDropdownMenuStyles(
                    leftIconDrawableId = R.drawable.ic_more
                )
            )
        }
    }
}