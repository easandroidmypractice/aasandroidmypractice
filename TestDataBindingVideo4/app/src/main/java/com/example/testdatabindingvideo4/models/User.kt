package com.example.testdatabindingvideo4.models

data class User(
    var id: Int,
    var name: String = "",
    var pass: String = ""
)
