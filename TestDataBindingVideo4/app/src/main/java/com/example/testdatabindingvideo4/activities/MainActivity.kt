package com.example.testdatabindingvideo4.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.testdatabindingvideo4.R
import com.example.testdatabindingvideo4.databinding.ActivityMainBinding
import com.example.testdatabindingvideo4.models.User

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        var u1 = User(1, "orlando", "123")

////        binding.user = u1

        //first option
        binding.apply {
            textView.setText("" + u1.id)
            nametxt.setText(u1.name)
            passwordTxt.setText(u1.pass)
        }
    }

//    override fun onDestroy() {
//        super.onDestroy()
//        //TO avoid memory leak we disallow the binding once the activity is destroyed
//        binding = null
//    }
}