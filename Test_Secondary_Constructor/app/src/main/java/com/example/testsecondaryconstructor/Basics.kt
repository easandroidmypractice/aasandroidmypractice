package com.example.testsecondaryconstructor

fun main() {
//    Student("nicholas", 5)

//    Constructors(6)

    val addressResult: Address = copyAddress(Address())
    println(addressResult.name)
    println(addressResult.street)
}

//class Student (var name: String) {
//    init {
//        println("Student has got a name as $name")
//    }
//
//    constructor(sectionName: String, id: Int) :this(sectionName) {
//    }
//}

class Student (var name: String) {
    var id: Int = -1
    init {
        println("Student has got a name as $name")
    }
    constructor(secname: String, id: Int) :this(secname) {
        this.id = id
        println("Student has got a name as $secname")
        println("it's id is: $id")
    }
}

class Constructors {
    init {
        println("Init block")
    }

    constructor(i: Int) {
        println("Constructor $i")
    }
}

class Address {
    var name: String = "Holmes, Sherlock"
    var street: String = "Baker"
    var city: String = "London"
    var state: String? = null
    var zip: String = "123456"
}

fun copyAddress(address: Address): Address {
    val result = Address() // there's no 'new' keyword in Kotlin
    result.name = address.name // accessors are called
    result.street = address.street
    return result
}
