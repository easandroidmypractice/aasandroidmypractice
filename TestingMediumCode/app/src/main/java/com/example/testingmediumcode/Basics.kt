package com.example.testingmediumcode

import kotlinx.coroutines.delay

// main function
fun main(args: Array<String>) {
//    print("GeeksforGeeks: ")
//    higher-order functions("A Computer Science portal for Geeks", ::print)
//************************
//    lambda()
//    lambda1()// normal code.
//    println("Main function starts")
//    inlinedFunc({ println("Lambda expression 1")
//                    return }, // inlined function allow return
//                              // statement in lambda expression
//                              // so, does not give compile time error
//                             { println("Lambda expression 2")} )
//
//    println("Main function ends")
//******************************
//    println(performOperation(2, 3) { number1, number2 ->
//        number1 * number2
//    })

//************************
//    val isSuccess = false
//    val message = isSuccess T "Success" F "Failure"
//    println(message)
//************************
//Creating a Coroutine With BuildSequence
//ex 1:
//    for (num in seq) {
//        println("The next number is $num")
//    }
////suspend fun
//    GlobalScope.launch {
//        val user = getUserSuspend("101")
//        println(user)
//    }
//    Thread.sleep(1500)
//************************
//use of also and run:
//    val message = "Hello, world!"
//    message.also {
//        println("The original message is: $it")//prints: The original message is: Hello, world!
//    }.run {
//        uppercase()
//    }.also {
//        println("The uppercase message is: $it")//prints: The uppercase message is: HELLO, WORLD!
//    }
//    println(message)//prints: Hello, world!
//************************
//use of let:
//    val message = "Hello, world!"
//    val result = message.let {
//        val reversedMessage = it.reversed()
//        reversedMessage.uppercase()
//    }
//    println(result) // output: "!DLROW ,OLLEH"
//
//    var message2: String? = null
////    message2 = "happy?"
//    val result2 = message2?.let {
//        val reversedMessage = it.reversed()
//        reversedMessage.uppercase()
//    }
//    println(result2) // output: null. because message2 is null
//************************
//use of apply:
//    val person = Person("John", 25)
//    person.apply {
//        name = "Jane"
//        age = 30
//    }
//    println(person) // output: "Person(name=Jane, age=30)"
//************************
//use of with:
//    val person = Person("John", 25)
//    val greeting = with(person) {
//        "Hello, $name! You are $age years old."
//    }
//    println(greeting) // output: "Hello, John! You are 25 years old."
//************************
//sumary of let, also, apply, run and with.
//// let
//    val name: String? = "John"
//    name?.let {
//        println("The length of the name is ${it.length}")
//    }

//// also
//    val list = mutableListOf<Int>()
//    val result = list.also {
//        it.add(1)
//        it.add(2)
//        it.add(3)
//    }.sum()
//    println("The result is $result")

//// apply//has syntax errors
//    val person = Person().apply {
//        name = "John"
//        age = 30
//    }
//    println("The person's name is ${person.name}, and age is ${person.age}")

//// run
//    val number = 42.run {
//        println("The number is $this")
//        this + 10
//    }
//    println("The result is $number")

//// with //has syntax errors
//    val user = User("John", "Doe")
//    with(user) {
//        println("The user's full name is $firstName $lastName")
//    }
//************************
////testing if vs let
//val ifVSledt: IfOrLet = IfOrLet()
//*********************************
////extracting substrings from a text:
//    val actorsList = extractInfo(text = "Three cats named 'Tabby', 'Tommy', and 'Terry', were sitting in a tree. They either had *white fur*, *brown fur*, or *black fur*, and were wearing collars that where either *red*, *pink*, or *orange*. Based on the clues, match the cat names with their fur and their collar colors.",
//        delimiterChar = "'")
////    extractInfo(story = "be_de=Interessant für Dich; be_fr=Intéressant pour toi;")
//*********************************
//a better for loop:
//    a()
//*********************************
////understanding di.
//    val engine = Engine()
//    val turbo = TurboEngine()
//    val car = Car(engine, turbo)
////    car.startCar()//before TurboEngine class.
//    car.engine.start()
//    car.turbo.start()
}
//*********************************************************************************************************************************************************************************
//higher-order functions:
//fun higherfunc( str : String, mycall :(String)-> Unit) {//this impacts on the memory and performance.
//    // invokes the print() by passing the string str
//    mycall(str)
//}

inline fun higherfunc( str : String, mycall :(String)-> Unit) {
    // invokes the print() by passing the string str
    mycall(str)
}

//*********************************
//var lambda = { println("Lambda expression")
//    return }      // normally lambda expression does not allow return
                    // statement, so gives syntax error
var lambda1 = { println("Lambda expression")}

// inlined function
inline fun inlinedFunc( lmbd1: () -> Unit, lmbd2: () -> Unit  ) {
    lmbd1()
    lmbd2()
}
//*********************************
inline infix fun <T> Boolean.T(trueOutput: T) = TernaryPart(this, trueOutput)

inline infix fun <T> TernaryPart<T>.F(falseOutput: T) = if (condition) trueOutput else falseOutput

class TernaryPart<T>(val condition: Boolean, val trueOutput: T)
//*********************************

fun performOperation(number1: Int, number2: Int, operation: (Int, Int) -> Int): Int { //the last Int is the function return type.
    return operation(number1, number2)
}
//***************************************************************
//Creating a Coroutine With BuildSequence
//ex 1:
val seq = sequence {
    println("Generating first")
    yield(1)
    println("Generating second")
    yield(2)
    println("Generating third")
    yield(3)
    println("Done")
}
////ex 2:
//val fibonacciSeq = sequence {//this is a lambda expression.
//    var a = 0
//    var b = 1
//
//    yield(1)
//
//    while (true) {
//        yield(a + b)
//
//        val tmp = a + b
//        a = b
//        b = tmp
//    }
//}
//*********************************
//suspend function
suspend fun getUserSuspend(userId: String): User {
    delay(1000)
    return User(userId, "ABHI")
}
class User(userId: String, name: String) {
}
//*********************************
//use of apply, with:
//data class Person(var name: String, var age: Int)
//*********************************
//testing if vs let:
class IfOrLet {

    private var string1: String? = "some string 1"

    init {
        if(string1 != null) {
            makeString1Null()
            println(string1) // prints null
        }
    }

    private fun makeString1Null() {
        string1 = null
    }
}
//*********************************
//extracting substrings from a text:
fun extractInfo(text: String, delimiterChar: String) :List<String>{
    var resultList = mutableListOf<String>()
    var index: Int = 0
    while (true) {
        var start: Int = text.indexOf(delimiterChar, index, false)
        if(start == -1) break
        var end: Int = text.indexOf(delimiterChar, start + 1, false)
        val result = text.substring(startIndex = start + 1, endIndex = end )
        resultList.add(result)
        index = ++end
        }
    return resultList
}
//*********************************
//a better for loop:
fun a() {
    repeat(9) { index ->
        println("some-string with index $index")
    }
}
//*********************************
//understanding di.
class Engine() {
    fun start() {
        println("Start with normal engine...")
    }
}

class TurboEngine(){
    fun start(){
        println("Start turbo engine x")
    }
}
//class Car(val engine: Engine) {
class Car(val engine: Engine, val turbo: TurboEngine) {//after TurboEngine class
//    val engine = Engine()//not good.it's not loose, it's connected to Engine object.
        fun startCar() {
//            println("starting car...${engine.start()}")//before TurboEngine class.
            println("starting car...")
        }
}

