package com.example.highlighttextview;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;

import com.example.highlighttextview.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //binding code:
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        String textStr = "In publishing and graphic design," +
                " \nLorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document" +
                " \nor a typeface without relying on meaningful content. " +
                "\n Lorem ipsum may be used as a placeholder before final copy is available.";

        binding.myTextView.setText(textStr);
        String splitStr[] = textStr.split("\n");
        Log.e("splitStr---",""+splitStr.length);
        Handler handler1 = new Handler();
//        for(int i = 0; i<splitStr.length; i++){
//            int finalI = i;
//            handler1.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    Spannable spannableString = new SpannableString(textStr);
//                    int startingPosition = textStr.indexOf(splitStr[finalI]);
//                    int endingPosition = startingPosition + splitStr[finalI].length();
//                    // For highlight current selected Sub String
//                    spannableString.setSpan(new ForegroundColorSpan(Color.MAGENTA), startingPosition, endingPosition, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//                    // For de-colorize the part of a string that has been colorized before
//                    spannableString.setSpan(new ForegroundColorSpan(Color.BLUE), 0, startingPosition, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//                    binding.myTextView.setText(spannableString);
//                }
//            }, 5000 * i);
//        }
        //this applies to the whole text.
            handler1.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Spannable spannableString = new SpannableString(textStr);
                    int startingPosition = 0;
                    int endingPosition = startingPosition + textStr.length();
                    // For highlight current selected Sub String
                    spannableString.setSpan(new ForegroundColorSpan(Color.MAGENTA), startingPosition, endingPosition, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    binding.myTextView.setText(spannableString);
                }
            }, 1000);

            handler1.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Spannable spannableString = new SpannableString(textStr);
                    int startingPosition = 0;
                    int endingPosition = startingPosition + textStr.length();
                    // For de-colorize the part of a string that has been colorized before
                    spannableString.setSpan(new ForegroundColorSpan(Color.BLACK), startingPosition, endingPosition, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    binding.myTextView.setText(spannableString);
                }
            }, 3000);
    }
}