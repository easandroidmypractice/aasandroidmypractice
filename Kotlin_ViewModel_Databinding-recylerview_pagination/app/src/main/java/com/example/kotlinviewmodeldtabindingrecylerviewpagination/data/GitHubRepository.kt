package com.example.kotlinviewmodeldtabindingrecylerviewpagination.data

import com.example.kotlinviewmodeldtabindingrecylerviewpagination.network.ApiEndPoint
import com.example.kotlinviewmodeldtabindingrecylerviewpagination.network.RetrofitClient

class GithubRepository {
    private val retrofit = RetrofitClient.getRetrofitInstance().create(ApiEndPoint::class.java)

    suspend fun getAllRepository(page : Int) = retrofit.getAllRepo(page)
}