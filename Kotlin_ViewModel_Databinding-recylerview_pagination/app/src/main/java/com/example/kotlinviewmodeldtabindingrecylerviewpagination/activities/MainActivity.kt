package com.example.kotlinviewmodeldtabindingrecylerviewpagination.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.kotlinviewmodeldtabindingrecylerviewpagination.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}