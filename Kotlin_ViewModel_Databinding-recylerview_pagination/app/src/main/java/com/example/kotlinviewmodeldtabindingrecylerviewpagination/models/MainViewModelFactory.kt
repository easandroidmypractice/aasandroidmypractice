package com.example.kotlinviewmodeldtabindingrecylerviewpagination.models

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.kotlinviewmodeldtabindingrecylerviewpagination.data.GithubRepository

class MainViewModelFactory : ViewModelProvider.Factory {
    @Throws(IllegalArgumentException::class)
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MainViewModel(
            repository = GithubRepository()
        ) as T
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}