package com.example.testing_viewpager2B.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import com.example.testing_viewpager2B.R;
import com.example.testing_viewpager2B.adapters.ViewPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/*
this uses viewPager2.
 */
public class MainActivity extends AppCompatActivity {
    ViewPager2 viewPager2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager2 = findViewById(R.id.viewPager2);

        List<String> list = new ArrayList<>();
        list.add("First Screen");
        list.add("Second Screen");
        list.add("Third Screen");
        list.add("Fourth Screen");

        viewPager2.setAdapter(new ViewPagerAdapter(this, list, viewPager2));

    }
}