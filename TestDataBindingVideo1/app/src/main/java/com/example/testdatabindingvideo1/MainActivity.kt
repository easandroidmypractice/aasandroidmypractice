package com.example.testdatabindingvideo1

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.testdatabindingvideo1.databinding.ActivityMainBinding
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        //first option:
//        binding.btn.setOnClickListener(){
//            var enteredText: String = binding.editText.text.toString()
//            binding.textView.setText("Hello $enteredText")

        //second option:
        binding.apply {
            btn.setOnClickListener(){
            var enteredText: String = editText.text.toString()
            textView.setText("Hello $enteredText")}
        }
    }

//    override fun onDestroy() {
//        super.onDestroy()
//        //TO avoid memory leak we disallow the binding once the activity is destroyed
//        binding = null
//    }
}