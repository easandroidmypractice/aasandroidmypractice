package com.example.testannotatedstrings

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.testannotatedstrings.components.AbstractArtTxt
import com.example.testannotatedstrings.components.ApplyingFontStyles
import com.example.testannotatedstrings.components.BackgroundHighlights
import com.example.testannotatedstrings.components.ClickableLink
import com.example.testannotatedstrings.components.ColorfulText
import com.example.testannotatedstrings.components.ExpandableTextExample
import com.example.testannotatedstrings.components.MyText
import com.example.testannotatedstrings.components.StylizedPoetry
import com.example.testannotatedstrings.components.VisualTransf1
import com.example.testannotatedstrings.components.VisualTransf2
import com.example.testannotatedstrings.screens.ContentView
import com.example.testannotatedstrings.ui.theme.TestAnnotatedStringsTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyApp{
                MainContent()
            }
        }
    }
}

@Composable
fun MyApp(content: @Composable () -> Unit) {
    TestAnnotatedStringsTheme {
        // A surface container using the 'background' color from the theme
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {
                    content()
        }
    }
}
@Preview
@Composable
fun MainContent() {
    ContentView()
//    MyText()
//    ApplyingFontStyles()
//    VisualTransf2()
//    VisualTransf1()
//    ColorfulText()
//    BackgroundHighlights()
//    ClickableLink()
//    ExpandableTextExample()
//    StylizedPoetry()
//    AbstractArtTxt()
}
