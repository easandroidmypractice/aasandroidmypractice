package com.example.testannotatedstrings

import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.SpanStyle

/**
 * Data class representing a link with its full text and a list of links.
 *
 * @property fullText The full text containing the link.
 * @property linksList A list of [Link] objects representing the links found in the full text.
 */
data class LinkData(
    val fullText: String,
    val linksList: List<Link>
)

/**
 * Represents a link within the text, with a display text, optional link information, a style, and an
 * action to perform when the link is clicked. Links can be used to navigate to other parts of the
 * application, open external URLs, or trigger other actions.
 *
 * @property linkText The text to display for the link. This is the text that users will see and interact with.
 * @property linkInfo Optional additional information about the link. This can be used to provide more context
 * or details about the link destination.
 * @property style The style to apply to the link text. This can be used to customize the appearance of the link,
 * such as the font, color, or underline.
 * @property onClick The action to perform when the link is clicked. This can be used to navigate to another
 * screen, open a URL, or trigger any other desired action.
 */
data class Link(
    val linkText: String,
    val linkInfo: String? = null,
    val style: SpanStyle,
    val onClick: (String) -> Unit
)

val linkData = LinkData(//everything in here will be overridden.
    fullText = "Some sample text with a link. Click here to visit profile screen.",
    linksList = listOf(
        Link(
            linkText = "link",
            linkInfo = "https://example.com/",
            style = SpanStyle(color = Color.Blue),
            onClick = { }
        ),
        Link(
            linkText = "Click here",
            linkInfo = "",
            style = SpanStyle(color = Color.Blue),
            onClick = { }
        )
    )
)