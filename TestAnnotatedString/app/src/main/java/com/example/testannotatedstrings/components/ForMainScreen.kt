package com.example.testannotatedstrings.components

import androidx.compose.foundation.text.ClickableText
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.buildAnnotatedString
import com.example.testannotatedstrings.LinkData

@Composable
fun TextWithLinks(
    modifier: Modifier = Modifier,
    linkData: LinkData,
    textStyle: TextStyle = MaterialTheme.typography.bodyLarge,) {

    val annotatedString = buildAnnotatedString {
        append(linkData.fullText)
        linkData.linksList.forEach { link ->
            var startIndex = linkData.fullText.indexOf(link.linkText)
            while (startIndex >= 0) {
                val endIndex = startIndex + link.linkText.length
                addStyle(
                    style = link.style,
                    start = startIndex,
                    end = endIndex
                )
                addStringAnnotation(
                    tag = link.linkText,
                    annotation = link.linkInfo ?: "",
                    start = startIndex,
                    end = endIndex
                )
                startIndex = linkData.fullText.indexOf(link.linkText, endIndex)
            }
        }
    }

    ClickableText(
        text = annotatedString,
        onClick = { position ->
            linkData.linksList.forEach { link ->
                annotatedString
                    .getStringAnnotations(link.linkText, position, position)
                    .firstOrNull()?.let {
                        link.onClick.invoke(it.item)
                        return@forEach//this is a continue
                    }
            }
        }
    )
}

