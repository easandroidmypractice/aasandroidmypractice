package com.example.testannotatedstrings.components

import android.annotation.SuppressLint
import android.widget.Toast
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.ExperimentalTextApi
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.UrlAnnotation
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.OffsetMapping
import androidx.compose.ui.text.input.TransformedText
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.testannotatedstrings.Link
import com.example.testannotatedstrings.LinkData

@OptIn(ExperimentalMaterial3Api::class)
@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun MyText() {
    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(text = "My Tests",)
                },
                modifier = Modifier.shadow(elevation = 5.dp),
                colors = TopAppBarDefaults.topAppBarColors(containerColor = Color.LightGray)
            )
        }
    ) {
        val context = LocalContext.current
        Column(
            modifier = Modifier.padding(start = 6.dp, top = 60.dp, end = 6.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ){
            val storyContent = "i am here for you"
            val linkText = "here"

            Card(modifier = Modifier
                .padding(4.dp)
                .fillMaxWidth()
                .height(600.dp),
                shape = RoundedCornerShape(corner = CornerSize(16.dp)),
                elevation = CardDefaults.cardElevation(defaultElevation = 6.dp)
            ){
                Surface(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(1.dp)
                        .fillMaxHeight()
                ){
                    Column(
                        modifier = Modifier.padding(12.dp),
                        horizontalAlignment = Alignment.CenterHorizontally,
                    ){
                        Spacer(Modifier.height(32.dp))
                        TextWithLinks(
                            textStyle = TextStyle(
                                fontSize = 24.sp,
                                textAlign = TextAlign.Justify),
                            linkData = LinkData(
//                                fullText = "Welcome to our app! For more information about our Privacy Policy, " +
//                                        "please review it carefully. If you're having trouble, check out " +
//                                        "our Help Center for FAQs and troubleshooting guides. Ready to get started? " +
//                                        "Head over to the Account Settings page to personalize your experience.",
                                fullText = "i am here for you",
                                linksList = listOf(
//                                    Link(
//                                        linkText = "Account Settings",
//                                        linkInfo = "https://www.example.com/account-settings",
//                                        style = SpanStyle(color = Color.Blue)
//                                    ) { str -> // This str will return the linkInfo of the link.
//                                        Toast.makeText(
//                                            context,
//                                            "Clicked on $str", //Toast will show -> Clicked on https://www.example.com/account-settings
//                                            Toast.LENGTH_LONG
//                                        ).show()
//                                    },
//                                    Link(
//                                        linkText = "Help Center",
//                                        style = SpanStyle(color = Color.Red),
//                                        linkInfo = "refund"
//                                    ) { str -> // This str will return the linkInfo of the link.
//                                        // navigateToHelpCenterScreen(helpTopic = str) will pass helpTopic = "refund"
//                                        Toast.makeText(
//                                            context,
//                                            "Clicked on $str", //Toast will show -> Clicked on refund
//                                            Toast.LENGTH_LONG
//                                        ).show()
//                                    },
                                    Link(
//                                        linkText = "Privacy Policy",
                                        linkText = "here",
                                        style = SpanStyle(color = Color.Magenta),
                                    ) {
                                        Toast.makeText(
                                            context,
                                            "Clicked on Privacy Policy",
                                            Toast.LENGTH_LONG
                                        ).show()
                                    }
                                )
                            )
                        )
                    }
                }
            }
        }
    }
}
//****************************************************************************
@OptIn(ExperimentalMaterial3Api::class)
@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun ApplyingFontStyles() {
    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(text = "My Tests",)
                },
                modifier = Modifier.shadow(elevation = 5.dp),
                colors = TopAppBarDefaults.topAppBarColors(containerColor = Color.LightGray)
            )
        }
    )
    {
        Column(
            modifier = Modifier.padding(start = 6.dp, top = 60.dp, end = 6.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        )
        {
            Card(modifier = Modifier
                .padding(4.dp)
                .fillMaxWidth()
                .height(300.dp),
                shape = RoundedCornerShape(corner = CornerSize(16.dp)),
                elevation = CardDefaults.cardElevation(defaultElevation = 6.dp)
            )
            {
                Surface(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(1.dp)
                        .fillMaxHeight()
                )
                {
                    Column(
                        modifier = Modifier.padding(12.dp),
                        horizontalAlignment = Alignment.CenterHorizontally,
                    )
                    {
                        val boldText = buildAnnotatedString {
                            withStyle(style = SpanStyle(fontWeight = FontWeight.Bold)) {
                                append("Bold Text")
                            }
                            append("\n\n")
                            append("Regular Text")
                        }
                        Text(text = boldText)
                    }
                }
            }
        }
    }
}
//****************************************************************************

@Composable
fun VisualTransf1() {
    var boldText: String = ""
    OutlinedTextField(
        value = boldText,
        onValueChange = {boldText = it},
        visualTransformation = {
            TransformedText(buildAnnotatedString(),
                OffsetMapping.Identity)
        })
}
fun buildAnnotatedString(): AnnotatedString{
    val boldText = buildAnnotatedString {
        withStyle(style = SpanStyle(fontWeight = FontWeight.Bold)) {
            append("Bold Text")
        }
        append("\n\n")
        append("Regular Text")
    }
    return boldText
}
//****************************************************************************
@Composable
fun VisualTransf2() {
    var text: String = "Hello World Hello World Hello World"
    OutlinedTextField(
        value = text,
        onValueChange = { text = it },
        visualTransformation = {
            TransformedText(
                buildAnnotatedStringWithColors(text),
                OffsetMapping.Identity
            )
        },
        label = {"Testing"}
    )
}

fun buildAnnotatedStringWithColors(text:String): AnnotatedString{
    val words: List<String> = text.split("\\s+".toRegex())// splits by whitespace
    val colors = listOf(Color.Red,Color.Black,Color.Yellow,Color.Blue)
    var count = 0

    val builder = AnnotatedString.Builder()
    for (word in words) {
        builder.withStyle(style = SpanStyle(color = colors[count%4])) {
            append("$word ")
        }
        count ++
    }
    return builder.toAnnotatedString()
}
//****************************************************************************
@Composable
fun ColorfulText() {
    var colorfulText: String = ""
    OutlinedTextField(
        value = colorfulText,
        onValueChange = {colorfulText = it},
        visualTransformation = {
            TransformedText(buildAnnotatedString2(),
                OffsetMapping.Identity)
        })
}
fun buildAnnotatedString2(): AnnotatedString{
    val colorfulText = buildAnnotatedString {
        withStyle(style = SpanStyle(color = Color.Red)) {
            append("Vibrant Text")
        }
        append("\n\n")
        append("Regular Text")
    }
    return colorfulText
}
//****************************************************************************
@Composable
fun BackgroundHighlights() {
    var highlightedText: String = ""
    OutlinedTextField(
        value = highlightedText,
        onValueChange = {highlightedText = it},
        visualTransformation = {
            TransformedText(buildAnnotatedString3(),
                OffsetMapping.Identity)
        })
}
fun buildAnnotatedString3(): AnnotatedString{
    val highlightedText = buildAnnotatedString {
        withStyle(style = SpanStyle(background = Color.Yellow)) {
            append("Highlighted Text")
        }
        append("\n\n")
        append("Regular Text")
    }
    return highlightedText
}
//****************************************************************************
@OptIn(ExperimentalTextApi::class)
@Composable
fun ClickableLink() {
    val annotatedLinkString: AnnotatedString = buildAnnotatedString {
        val str = "Let's open google!"
        val startIndex = str.indexOf("google")
        val endIndex = startIndex + 6
        append(str)
        addStyle(
            style = SpanStyle(
                color = Color.Red,
                textDecoration = TextDecoration.Underline
            ), start = startIndex, end = endIndex
        )
        addUrlAnnotation(
            UrlAnnotation("https://google.com"),
            start = startIndex,
            end = endIndex
        )
    }

    val uriHandler = LocalUriHandler.current
    ClickableText(
        modifier = Modifier.padding(20.dp).fillMaxWidth(),
        text = annotatedLinkString,
        onClick = {
            annotatedLinkString
                .getUrlAnnotations(it, it)
                .firstOrNull()?.let { annotation ->
                    uriHandler.openUri(annotation.item.url)
                }
        }
    )
}
//****************************************************************************
@Composable
fun ExpandableTextExample() {
    // Define tags for expanded and minified text
    val tagExpanded = "expanded_text"
    val tagMinified = "minified_text"
// Maintain the state of text toggling (expanded or minified)
    val textToggleState = remember { mutableStateOf(Pair(tagMinified, "Read more ...")) }
// Create the annotated string for expanded and minified text
    val expandedTextString: AnnotatedString = buildAnnotatedString {
        /* textToggleState.value.second takes the second argument of the Pair()
        it means "Read more ..."
         */
        val toggleString = textToggleState.value.second

        // Define the base article snippet with a toggle string
        val snippet = "In a groundbreaking discovery, scientists " +
                "have identified a new species of marine life " +
                "in the deep sea. $toggleString"

        // Find the start and end indices of the toggle string
        val startIndex = snippet.indexOf(toggleString)
        val endIndex = startIndex + toggleString.length

        // Apply styling to the entire snippet (font size and semi-bold)
        withStyle(style = SpanStyle(fontSize = 24.sp)) {
            withStyle(style = SpanStyle(fontWeight = FontWeight.SemiBold)) {
                append(snippet)
            }
            // If the text is expanded, add more article content
            if(textToggleState.value.first == tagExpanded) {
                append(
                    "\n\nThis new species, tentatively named " +
                            "'DeepSea Marvel,' was found at a depth " +
                            "of 4,000 meters beneath the ocean's surface."
                )
            }
        }

        // Apply styling to the specified range (magenta color and underline)
        addStyle(
            style = SpanStyle(
                color = Color.Magenta,
                textDecoration = TextDecoration.Underline
            ),
            start = startIndex, end = endIndex
        )

        // Add annotations based on the text state (expanded or minified)
        if(textToggleState.value.first == tagExpanded) {
            addStringAnnotation(
                tagMinified,
                "Read again ...",
                start = startIndex,
                end = endIndex
            )
        } else {
            addStringAnnotation(
                tagExpanded,
                "Show less ...",
                start = startIndex,
                end = endIndex
            )
        }
    }
// Create a clickable text composable to display the article text
    ClickableText(
        modifier = Modifier
            .padding(16.dp)
            .fillMaxWidth(),
        text = expandedTextString,
        onClick = {
            // Toggle between expanded and minified text based on annotations
            expandedTextString
                .getStringAnnotations(it, it)
                .firstOrNull()?.let { annotation ->
                    if(annotation.tag == tagExpanded) {
                        textToggleState.value = Pair(tagExpanded, annotation.item)
                    } else {
                        textToggleState.value = Pair(tagMinified, annotation.item)
                    }
                }
        }
    )
}
//****************************************************************************
@Composable
fun StylizedPoetry() {
    var stylizedPoetry: String = ""
    OutlinedTextField(
        value = stylizedPoetry,
        onValueChange = {stylizedPoetry = it},
        visualTransformation = {
            TransformedText(buildAnnotatedString4(),
                OffsetMapping.Identity)
        })
}
fun buildAnnotatedString4(): AnnotatedString{
    val stylizedPoetry = buildAnnotatedString {
        withStyle(style = SpanStyle(fontSize = 24.sp)) {
            append("The ")
            withStyle(style = SpanStyle(fontWeight = FontWeight.Bold, color = Color.Red)) {
                append("rose")
            }
            append(" is ")
            withStyle(style = SpanStyle(fontStyle = FontStyle.Italic, color = Color.Green)) {
                append("red")
            }
            append("\n")
            withStyle(
                style = SpanStyle(
                    textDecoration = TextDecoration.LineThrough,
                    color = Color.Blue
                )
            ) {
                append("The ")
            }
            withStyle(style = SpanStyle(fontStyle = FontStyle.Italic, color = Color.Red)) {
                append("violets")
            }
            append(" are blue")
        }
    }
    return stylizedPoetry
}
//****************************************************************************
@Composable
fun AbstractArtTxt() {
    var abstractArtText: String = ""
    OutlinedTextField(
        value = abstractArtText,
        onValueChange = {abstractArtText = it},
        visualTransformation = {
            TransformedText(buildAnnotatedString5(),
                OffsetMapping.Identity)
        })
}
fun buildAnnotatedString5(): AnnotatedString{
    val abstractArtText = buildAnnotatedString {
        withStyle(
            style = SpanStyle(
                fontSize = 42.sp,
                brush = Brush.horizontalGradient(listOf(Color.Red, Color.Blue, Color.Green))
            )
        ) {
            append("Colors in Motion")
        }
        append("\n\n")
        append("Vivid shades of ")
        withStyle(
            style = SpanStyle(
                brush = Brush.verticalGradient(
                    listOf(
                        Color.Magenta,
                        Color.Yellow
                    )
                )
            )
        ) {
            append("passion")
        }
        append(" blend with the ")
        withStyle(
            style = SpanStyle(
                brush = Brush.horizontalGradient(
                    listOf(
                        Color.Blue,
                        Color.Green
                    )
                )
            )
        ) {
            append("calmness")
        }
        append(" of an eternal ")
        withStyle(
            style = SpanStyle(
                brush = Brush.radialGradient(
                    listOf(
                        Color.Red,
                        Color.Magenta
                    )
                )
            )
        ) {
            append("sunrise")
        }
        append(". A symphony of ")
        withStyle(style = SpanStyle(brush = Brush.linearGradient(listOf(Color.Blue, Color.Cyan)))) {
            append("colors")
        }
        append(" dances in the ")
        withStyle(
            style = SpanStyle(
                brush = Brush.sweepGradient(
                    listOf(
                        Color.Green,
                        Color.Blue,
                        Color.Magenta
                    )
                )
            )
        ) {
            append("sky")
        }
        append(" like a dream.")
    }
    return abstractArtText
}
//****************************************************************************