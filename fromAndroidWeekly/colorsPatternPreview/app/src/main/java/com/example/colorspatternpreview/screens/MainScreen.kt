package com.example.colorspatternpreview.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

val Red = Color(0xFFFF0000)
val Green = Color(0xFF00FF00)
val Blue = Color(0xFF0000FF)

@Preview
@Composable
fun ThemeColorsPreview() {
    Column(
        verticalArrangement = Arrangement.spacedBy(8.dp)
    ) {
        ColorBox(color = Red, name = ::Red.name)
        ColorBox(color = Green, name = ::Green.name)
        ColorBox(color = Blue, name = ::Blue.name)
    }
}

@Composable
fun ColorBox(color: Color, name: String) {
    Box(modifier = Modifier
        .size(48.dp)
        .background(color),
        contentAlignment = Alignment.Center,
    ) {
        Text(text = name)
    }
}